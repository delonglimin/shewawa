class Helper {
    /** 不包括max*/
    public static Random(min: number, max: number): number {
        if (min == max) return max;

        let randomNum = Math.random() * (Math.abs(max - min)) + (max > min ? min : max);
        return Math.floor(randomNum);
    }

    /** 包括max*/
    public static RandomContainMax(min: number, max: number): number {
        if (min == max) return max;

        return this.Random(min, max + 1);
    }

    public static StrEmptyOrNull(str: string): boolean {
        return str == null || str == "";
    }

    public static SetImageCrossOrigin(img: eui.Image, url: string) {
        if (url == "default.png" || url == HttpManager.URL + "default.png") {
            img.source = "default_avtar_png";
            return;
        }

        let flag: string = url.slice(0, 4);
        if (flag != "http") {
            url = HttpManager.cdnURL + url;
        }
        
        let imageLoader: egret.ImageLoader = new egret.ImageLoader();
        imageLoader.crossOrigin = "anonymous";
        imageLoader.addEventListener(egret.Event.COMPLETE, () => { img.bitmapData = imageLoader.data; }, this);

        try {
            imageLoader.load(url);
        } catch (e) {
            Toast.launch("imageLoader load error: " + url);
            img.source = "default_avtar_png";
        }
    }

    /**娃娃图片 添加圆角遮罩 */
    public static ImageAddMask(img: eui.Image) {
        if (!img || img.mask) return;

        let texture = RES.getRes('bg_3_png') as egret.Texture;
        let textureHeight = texture.textureHeight;
        let textureWidth = texture.textureWidth;
        let space = 2;

        let mask = new eui.Image(texture);
        mask.scale9Grid = new egret.Rectangle((textureWidth - space) * .5, (textureHeight - space) * .5, space, space);
        mask.height = img.height;
        mask.width = img.width;

        img.parent.addChild(mask);
        mask.top = img.top;
        mask.bottom = img.bottom;
        mask.right = img.right;
        mask.left = img.left;
        mask.verticalCenter = img.verticalCenter;
        mask.horizontalCenter = img.horizontalCenter;
        mask.x = img.x;
        mask.y = img.y;

        img.mask = mask;
    }
}