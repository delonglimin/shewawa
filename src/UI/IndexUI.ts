class IndexUI extends BaseUI{
	private mScroller:eui.Scroller;
	private mList:eui.List;
	private mDataArray:Array<Object> = [];
	private IndexContainer:eui.Group;
	private mListArrayCollection: eui.ArrayCollection;
	public constructor() {
		super();
		
	}
	

	public GetExmlName(){return "IndexUI.exml";}

	public Open(){
		super.Open();
		this.SetDataArray();
		this.Refresh();
	}

	public Close(){
		super.Close();
		this.mDataArray = [];
	}


	private SetDataArray(){
		this.mDataArray = [];
		// let data = {
        //          desc: {
        //             "name":"Apple/苹果 iPhone X",
        //             "detail":"银色，深空灰色",
        //             "transportInfo":"随机发货",
        //             img :"http://103.244.233.243/data/upload/20180216/5a86ce389d2e6.jpg"
        //          },
        //         id : "143",
        //         blood:"3",
        //         name :"Apple/苹果 iPhone X",
        //         shoot_num:11,//射中次数
        //         thumb :"http://103.244.233.243/data/upload/20180211/5a7fa5c48e238.png",
        //         price:"10"
        // };

		// for(let i = 0; i  < 10; i++){
		// 	this.mDataArray.push(data);
		// }
		this.mListArrayCollection = new eui.ArrayCollection(this.mDataArray);
		this.mList.dataProvider = this.mListArrayCollection;
		this.mList.itemRenderer = IndexListItemUI;
	}

	private Refresh(){
		this.RequestShootDollsList();
	}

	private RequestShootDollsList() {
		let params = {};
		HttpManager.SendPost(HttpManager.GET_SHOOT_DOLL, this, params, (data) => {
			let code = data.code;
			if (code === 200) {
				let dollData = data['data'];
				this.mDataArray = [];
				for (let item of dollData) {
					this.mDataArray.push(item);
				}
				this.mListArrayCollection.replaceAll(this.mDataArray);
			} else {
				Toast.launch(data.descrp);
			}

		});
	}
	

}

class IndexListItemUI extends eui.ItemRenderer {
	private mIcon: eui.Image;
	private mItemName: eui.Label;
	private mCostNum: eui.Label;
    private mNum: eui.Label;
	constructor() {
		super();
		this.skinName = "IndexListItem";
	}
    protected createChildren() {
		super.createChildren();

		this.mIcon.addEventListener(egret.TouchEvent.TOUCH_TAP, (evt) => {
            UIManager.OpenUI(UIName.ShootDollUI, this.parent.parent.parent, (openUI: BaseUI) => {
                if (openUI) {
                    openUI.SetData("shootDollData", this.data);
                }
            });
		}, this);
	}

	protected dataChanged() {
		let icon:string = this.data.desc.img;
		let name:string = this.data.name;
		let cost:string = this.data.price;
        let num:string = `射中${this.data.shoot_num}次`;
		this.mItemName.text = name;
		this.mCostNum.text = cost;
        this.mNum.text = num;
        Helper.SetImageCrossOrigin(this.mIcon, icon);
	}
}