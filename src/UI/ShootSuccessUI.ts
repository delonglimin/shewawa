class ShootSuccessUI extends BaseUI {

	private mCloseBtn:eui.Button;
	private mIcon:eui.Image;

	private timer: egret.Timer;

	private particle1:particle.GravityParticleSystem;
	private particle2:particle.GravityParticleSystem;
	private particle3:particle.GravityParticleSystem;
	private particle4:particle.GravityParticleSystem;
	private particle5:particle.GravityParticleSystem;
	private particle6:particle.GravityParticleSystem;
	private particle7:particle.GravityParticleSystem;
	private particle8:particle.GravityParticleSystem;
	private particle9:particle.GravityParticleSystem;
	private particle10:particle.GravityParticleSystem;

	private countDownValue:number = 10;

    public constructor() { super(); }

    public GetExmlName(): string { return "ShootSuccessSkin.exml"; }


    protected RegisterBtnEvt() {
		this.mCloseBtn.once(egret.TouchEvent.TOUCH_TAP, this.CloseBtn, this);
    }

	private CloseBtn() {
		this.CloseParticle();
		this.BackBtn();
	}

	public Open()
	{
		super.Open();
		let imageurl = this.GetData("imageURL");
        if(imageurl){
			Helper.SetImageCrossOrigin(this.mIcon,imageurl.toString());
			Helper.ImageAddMask(this.mIcon);
        } 
		this.countDownValue = 10;
		this.timer = new egret.Timer(1000, 10);
        this.timer.addEventListener(egret.TimerEvent.TIMER, this.TimeCountDown, this);
        this.timer.start();

		//this.particle1 =  ParticleManager.CreateParticle(this.parent,318,500,"bg2_png","bg_json");
		this.particle2 =  ParticleManager.CreateParticle(this,318,0,"cheng_png","cheng_json");
		this.particle3 =  ParticleManager.CreateParticle(this,318,50,"dian_png","dian_json");
		this.particle4 =  ParticleManager.CreateParticle(this,318,0,"hong_png","hong_json");
		this.particle5 =  ParticleManager.CreateParticle(this,318,0,"huang_png","huang_json");
		this.particle6 =  ParticleManager.CreateParticle(this,318,0,"lan_png","lan_json");
		this.particle7 =  ParticleManager.CreateParticle(this,318,0,"lv_png","lv_json");
		this.particle8 =  ParticleManager.CreateParticle(this,318,0,"qing_png","qing_json");
		this.particle9 =  ParticleManager.CreateParticle(this,318,50,"xx_png","xx_json");
		this.particle10 =  ParticleManager.CreateParticle(this,318,0,"zi_png","zi_json");

		//this.particle1.start();
		this.particle2.start();
		this.particle3.start();
		this.particle4.start();
		this.particle5.start();
		this.particle6.start();
		this.particle7.start();
		this.particle8.start();
		this.particle9.start();
		this.particle10.start();	
	}

	private TimeCountDown() {
		this.countDownValue -= 1;
		if (this.countDownValue === 0)
		{
			this.CloseParticle();
			this.BackBtn();
		}		
	}


	private CloseParticle()
	{
		this.timer.stop();
		//this.particle1.stop();
		this.particle2.stop();
		this.particle3.stop();
		this.particle4.stop();
		this.particle5.stop();
		this.particle6.stop();
		this.particle7.stop();
		this.particle8.stop();
		this.particle9.stop();
		this.particle10.stop();	
	}

}
