class CoinNotEnoughUI extends BaseUI {
	private mBackBtn: eui.Button;
	private RechargeBtn: eui.Button;

	public constructor() {
		super();
	}

	public GetExmlName() { return "CoinNotEnough.exml"; }

	protected RegisterBtnEvt() {
		this.mBackBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.BackBtn, this);
		this.RechargeBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.GoRecharge, this);
	}

	protected RemoveBtnEvt() {
		this.mBackBtn.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.BackBtn, this);
		this.RechargeBtn.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.GoRecharge, this);
	}


	private GoRecharge() {
		this.BackBtn();
		}
}