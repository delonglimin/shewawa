class DefineUI {
	public constructor() {
	}
}

const mcCustomSkinsPath: string = "resource/custom_skins/";

class UIName {
	static DiamondRecordUI: string = "DiamondRecordUI";
	static EditAddressUI: string = "EditAddressUI";
	static FeedbackUI: string = "FeedbackUI";
	static GiftCodeInputUI: string = "GiftCodeInputUI";
	static GiftCodeUI: string = "GiftCodeUI";
	static GrabRecordUI: string = "GrabRecordUI";
	static NotifyUI: string = "NotifyUI";
	static LoginUI: string = "LoginUI";
	static PayUI: string = "PayUI";
	static QuestionsUI: string = "QuestionsUI";
	static AboutUI: string = "AboutUI";
	static FooterUI: string = "FooterUI";
	static MainUI: string = "MainUI";
	static SettingUI: string = "SettingUI";
	static PlayerInfoUI: string = "PlayerInfoUI";
	static VideoDisplayUI: string = "VideoDisplayUI";
	static GrabDollUI: string = "GrabDollUI";
	static RecordListUI: string = "RecordListUI";
	static FailTryAgainUI: string = "FailTryAgainUI";
	static SignInUI: string = "SignInUI";
	static SucessGrabDoll: string = "SucessGrabDoll";
	static DollDetailUI: string = "DollDetailUI";
	static ShopUI: string = "ShopUI";
	static GoodsInfoUI: string = "GoodsInfoUI";
	static ExchangeRecordUI: string = "ExchangeRecordUI";
	static ExchangeBigDollUI: string = "ExchangeBigDollUI";
	static ExchangeBigDollRecordUI: string = "ExchangeBigDollRecordUI";
	static ExchangeBigDollDetailUI: string = "ExchangeBigDollDetailUI";
	static TransportSkinUI: string = "ExchangeRecordUI";
	static ShootDollUI: string = "ShootDollUI";
	static ShootRecordUI: string = "ShootRecordUI";
	static ShootSuccessUI: string = "ShootSuccessUI";
	static ShopUI1: string = "ShopUI1";
	static ShopUI2: string = "ShopUI2";
	static ShopUI3: string = "ShopUI3";
	static ShopUI4: string = "ShopUI4";
	static ShopUI5: string = "ShopUI5";
	static GuideUI: string = "GuideUI";
	static BannerUI: string = "BannerUI";
	static CoinNotEnoughUI: string = "CoinNotEnoughUI";
	static ScoreInfoUI: string = "ScoreInfoUI";
	static GetGiftUI: string = "GetGiftUI";
	static postageUI: string = "postageUI";
	static BuyDollUI: string = "BuyDollUI";
	static WeekMemberUI: string = "WeekMemberUI";
	static CouponUI: string = "CouponUI";
	static RrewardFristUISkin: string = "RrewardFristUISkin";
	static GiftInfoUI: string = "GiftInfoUI";
	static ShootGiftInfoUI: string = "ShootGiftInfoUI";
	static DaShenSkinUI: string = "DaShenSkinUI";
	static StartGameTipUI: string = "StartGameTipUI";
	static GiftRandomInfo: string = "GiftRandomUIInfo";
	static ShootGiftRandomInfo: string = "ShootGiftRandomInfo";
	static SharePhotoUI:string  = "SharePhotoUI";
	static GameHallUI: string = "GameHallUI";
	static MainShootUI:string  = "MainShootUI";
	static MainGrabUI:string  = "MainGrabUI";
	static IndexUI:string  = "IndexUI";
	
}

enum ePayType {
	AliPay = 1,
	WeChatAppPay = 2,
	WeChatWebPay = 12,
}

enum eFooterType {
	Doll,
	Pay,
	Hall,
	PlayerInfo,
	Shop,
}

enum eTransportType {
	All = -1,
	Waitting,
	Going,
	Arrived,
}

class OpenIndex {
	static OpenIndex: number = -1;
}