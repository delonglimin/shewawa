abstract class BaseUI extends eui.Component{
	public mbIsOpen:boolean = false;
	private mData:Object = {};
	protected mCurrentChildUI: BaseUI;

	public constructor() {
		super();
		// this.addEventListener(eui.UIEvent.ADDED_TO_STAGE, this.Open, this);
		this.skinName = mcCustomSkinsPath + this.GetExmlName();
	}

	public abstract GetExmlName():string;

	public Open(){
		//egret.log('OpenUI >>> ' + this.GetExmlName());
		this.RegisterBtnEvt();
		this.RegisterGameEvt();
		this.mbIsOpen = true;
		this.width = this.stage.stageWidth;
		this.height = this.stage.stageHeight;
	}

	public Close(){
		this.mbIsOpen = false;
		this.RemoveBtnEvt();
		this.RemoveGameEvt();
		this.mData = {};
		this.RemoveCurrentChildUI();
	}

	protected RegisterBtnEvt(){}
	protected RemoveBtnEvt(){}

	protected RegisterGameEvt(){}
	protected RemoveGameEvt(){}

	public SetData(key:string, value:Object){
		this.mData[key] = value;
	}

	public GetData(key:string):Object{
		let tempObj = this.mData[key];
		if(!tempObj){egret.log(`${this.GetExmlName()}; GetData(${key}): undefined`);};
		return tempObj;
	}

	protected BackBtn(){
		UIManager.CloseUI(this);
	}

	public SetCurrentChildUI(childrenUI){
		this.mCurrentChildUI = childrenUI;
	}

	public RemoveCurrentChildUI() {
		if (this.mCurrentChildUI) {
			UIManager.CloseUI(this.mCurrentChildUI);
			this.mCurrentChildUI = null;
		}
	}

}