 class UIManager{
	public static msUIObjArray: Array<Object> = [];

	public static GetUIObj(name:string):BaseUI{
		let uiObj:BaseUI = null;
		for(let v of this.msUIObjArray){
			if(v["name"] === name){
				uiObj = v["uiObj"];
				break;
			}
		}

		if(!uiObj){
			uiObj = this.NewUIObj(name);
			if(uiObj){
				this.msUIObjArray.push({name: name, uiObj: uiObj});
			}else{
				console.log("classUI is null >>> " + name);
			}
		}
		
		return uiObj;
	}

	public static OpenUI(name:string, parent:egret.DisplayObjectContainer, setDataFunc?:Function):BaseUI{
		if(!parent) return;
		let openUI = this.GetUIObj(name);
		if(setDataFunc){
			setDataFunc(openUI);
		}
		parent.addChild(openUI);
		openUI.Open();
		return openUI;
	}

	public static CloseUIByName(name:string){
		let uiObj:BaseUI = null;
		for(let l of this.msUIObjArray){
			if(l["name"] === name){
				uiObj = l["ClassUI"];
				break;
			}
		}

		if(uiObj) this.CloseUI(uiObj);
	}

	public static CloseUI(uiObj:BaseUI){
		if(!uiObj || !uiObj.mbIsOpen) return;
		uiObj.Close();

		if(uiObj.parent){
			uiObj.parent.removeChild(uiObj);
		}
	}

	private static NewUIObj(name:string):BaseUI{
		let clazz = egret.getDefinitionByName(name);
		return new clazz();
	}

}