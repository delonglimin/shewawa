class UICommon {
	public static GetShootDeviceIDByDollID(dollID:number):number{
		return 1000000 + dollID;
	}

	public static HandleName(name: string): string {
        if (name.length > 2) {
            name = name.slice(0, 5) + "...";
        }
        return name;
    }

}