class ShootDollUI extends BaseUI {
    private mScroller: eui.Scroller;
    private mBackBtn: eui.Button;
    private mOnlineNumLabel: eui.Label;
    private mAudiencesInfo: eui.Group;
    private mAudienceAvatar: eui.Image;
    private mAudienceAvatarMask: eui.Image;
    private mAudienceAvatar0: eui.Image;
    private mAudienceAvatarMask0: eui.Image;
    private mAudienceAvatar1: eui.Image;
    private mAudienceAvatarMask1: eui.Image;
    private mDiamondIcon: eui.Image;
    private mDiamondNum: eui.Label;
    private mUserInfo: eui.Group;
    private mUsingDes: eui.Label;
    private mUserHeadMask: eui.Image;
    private mUserHead: eui.Image;
    private mCostNum: eui.Label;
    private mRecordBtn: eui.Button;
    private mPayBtn: eui.Button;
    private mDollDetailBtn: eui.Button;
    private mChatBtn: eui.Button;
    private mChatContent: eui.Label;
    private mChatContentBg: eui.Image;
    private mChatScroller: eui.Scroller;
    private mChatVisibleToggle: eui.ToggleButton;
    private mChatTextFlow: Array<egret.ITextElement>;
    private mChatMaxCount = 10;
    private mChatHandledInfo: eui.Group;
    private mChatPhraseInfo: eui.Group;
    private mChatphraseScroller: eui.Scroller;
    private mChatPhraseList: eui.List;
    private mChatSendBtn: eui.Button;
    private mChatPhraseVisibleToggle: eui.ToggleButton;
    private mChatInput: eui.TextInput;
    private mCloseChatPhraseBtn: eui.Button;
    private mShootDollData: ShootDollData;
    // private mSameScroller: eui.Scroller;
    // private mSameList: eui.List;
    // private mSameLeftBtn: eui.Button;
    // private mSameRightBtn: eui.Button;
    // private mSameFreeCount: eui.Label;
    // private mSameTotalCount: eui.Label;
    private mDollDetailGroup: eui.Group;
    private mDollName: eui.Label;
    //private mDollLength: eui.Label;
    private mDollDes: eui.Label;
    private mDollTransportInfo: eui.Label;
    private mDollAvatar: eui.Image;
    private mRecordGroup: eui.Group;
    private mRecordList: eui.List;
    private mRotateCircle: eui.Image;
    private mRotationDes: eui.Label;
    private mBulletStartPoint: eui.Image;
    private mBullet: eui.Group;
    private mBulletStar: eui.Image;
    private mBulletTweening: boolean = false;
    private mBulletDragEffect: particle.GravityParticleSystem;
    private mbShoot: boolean = false;
    private mShootBtn: eui.Button;
    private mShootAvatar: eui.Image;
    private mShootAvatarInitPosY: number;
    private mMonsterTotalHp = 3;
    private mMonsterCurHp = 3;
    private mHpCollection: eui.ArrayCollection;
    private mHpList: eui.List;
    private mBulletStartX = 0;
    private mBulletStartY = 0;
    private mThroughCircleMinAngle = 0;
    private mThroughCircleMaxAngle = 0;
    private mCircleCurAngle = 0;
    private mCircleDefaultAddAngle = 3 / 16;
    private mCircleAddAngle = 3 / 16;
    private mCircleAddAngleRate = 1;
    // private mBulletCurAngle = 0;
    private mBulletDefaultAddPosY = 7.5 / 16;
    private mBulletAddPosY = 7.5 / 16;
    private mBulletAddAngle = 10 / 16;
    private mBulletTargetPosY = 0;
    private mBulletMoveDistanceForCalculation = 0;
    // private mBulletCurTickCount = 0;
    private mBulletDelayTickCount = 0;
    private mBulletDelayMaxTickCount = 20 / 60 * 1000;
    private mStarToggle: eui.ToggleButton;
    private mCostIcon: eui.Image;
    private mShootNum: eui.Label;
    private mLineParent: eui.Group;
    private mbLineTweenToLeft: boolean = false;
    private mHitEffectPoint: particle.GravityParticleSystem;
    private mHitEffectStar: particle.GravityParticleSystem;
    private mHitSetTimeOut: number;
    private mOnlineNum = 0;
    private mOnlineUserAvatarArray = [];
    private mID = "";
    private mName = "";
    private mAvatar = "";
    private mDeviceID = 0;
    private mRemoteUserID = 0;
    private mChannel: any = 0;
    private game_log_id = "";
   // private mSocket: SocketIOClient.Socket;
    private NORMAL = 1;
    private agoraSession: any;
    private shootGiftGroup: eui.Group;
    private needShoot: eui.Label;
    private giftDes: eui.Label;
    private giftName: eui.Label;
    private giftPrice: eui.Label;
    private itemicon: eui.Image;
    private itemicon2: eui.Image;
    private priceImage: eui.Image;
    private mTimeEnterFrame: number;
    private bg: eui.Image;
    private bg2: eui.Image;
    private ShareBtn: eui.Image;
    private mbGuide: boolean = true;
    private mGuideGroup: eui.Group;
    private mCloseGuideBtn: eui.Button;
    private particle2: particle.GravityParticleSystem;
    private particle3: particle.GravityParticleSystem;
    private particle4: particle.GravityParticleSystem;
    private particle5: particle.GravityParticleSystem;
    private particle6: particle.GravityParticleSystem;
    private particle7: particle.GravityParticleSystem;
    private particle8: particle.GravityParticleSystem;
    private particle9: particle.GravityParticleSystem;
    private particle10: particle.GravityParticleSystem;
    private mRecoveryHP_Time: eui.Label;
    private mRecoveryHP_Text: eui.Label;

    //==============================login===========================================
    public constructor() { super(); }
    public GetExmlName(): string { return "ShootDollSkin.exml"; }

    protected RegisterBtnEvt() {
        this.mBackBtn.once(egret.TouchEvent.TOUCH_TAP, this.BackBtn, this);
        this.mRecordBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.RecordBtn, this);
        this.mChatBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.ChatBtn, this);
        this.mChatVisibleToggle.addEventListener(egret.TouchEvent.TOUCH_TAP, this.ChatVisibleToggle, this);
        this.mChatSendBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.ChatSendBtn, this)
        this.mChatPhraseVisibleToggle.addEventListener(egret.TouchEvent.TOUCH_TAP, this.ChatPhraseVisibleToggle, this);
        this.mCloseChatPhraseBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.CloseChatHandleBtn, this);
        this.mPayBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.PayBtn, this);
        this.mDollDetailBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.DollDetailBtn, this);
        this.mStarToggle.addEventListener(egret.TouchEvent.TOUCH_TAP, this.StarToggle, this);
        this.mShootBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.ShootBtn, this);
        // this.mSameList.addEventListener(eui.ItemTapEvent.ITEM_TAP, this.ClickSameItem, this);
        // this.mSameRightBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.SameBtn, this);
        // this.mSameLeftBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.SameBtn, this);
        this.ShareBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.OpenShare, this);

        this.mCloseGuideBtn.once(egret.TouchEvent.TOUCH_TAP, this.CloseGuide, this);
    }

    protected RemoveBtnEvt() {
        this.mRecordBtn.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.RecordBtn, this);
        this.mChatBtn.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.ChatBtn, this);
        this.mChatVisibleToggle.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.ChatVisibleToggle, this);
        this.mChatSendBtn.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.ChatSendBtn, this)
        this.mChatPhraseVisibleToggle.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.ChatPhraseVisibleToggle, this);
        this.mCloseChatPhraseBtn.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.CloseChatHandleBtn, this);
        this.mPayBtn.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.PayBtn, this);
        this.mDollDetailBtn.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.DollDetailBtn, this);
        this.mStarToggle.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.StarToggle, this);
        this.mShootBtn.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.ShootBtn, this);
        // this.mSameList.removeEventListener(eui.ItemTapEvent.ITEM_TAP, this.ClickSameItem, this);
        // this.mSameRightBtn.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.SameBtn, this);
        // this.mSameLeftBtn.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.SameBtn, this);
        this.ShareBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.OpenShare, this);
    }

    protected RegisterGameEvt() {
        this.addEventListener(egret.Event.ENTER_FRAME, this.EnterFrame, this);
        // egret.startTick(this.StartTick, this);
    }

    protected RemoveGameEvt() {
        this.removeEventListener(egret.Event.ENTER_FRAME, this.EnterFrame, this);
        // egret.stopTick(this.StartTick, this);
    }
//open--registerBtn registerGame ---init
    public Open() {
        super.Open();
        this.Init();//初始化游戏信息 开始绳子动画
        this.InitRotateInfo();//初始化子弹数据和旋转的环 距离   角度设定等信息
        //this.VisibleGuide();//初始引导
        this.DisplayShootInfo();//显示要射击的图片
        this.InitMonsterHpInfo();//显示血量信息
        this.getCurMonsterHp();
        //this.DisplayStarStatus();//显示是否收藏
        this.DisplayDiamondNum();//显示用户余额等信息  是金币还是娃娃券
        this.DisplayCostNum();//显示需要花费多少金币 或者券
       // this.DisplaySameInfo();//显示空闲   同类型机器
        this.RequestDollDetailInfo();//获取并显示奖品详情
        //this.InitBarrage();//初始化房间弹幕
        //this.InitAudienceAvatar();//初始化观众信息  在线人数 和前三位
       // this.RefreshChatDisplayedInfo();//聊天信息显示收藏控制
        this.VisibleChatHandledInfo(false);//聊天助手显示隐藏
        this.VisibleDollDetailInfo(true);//显示娃娃详情
        this.VisibleRecordGroup(false);//房间记录
        this.DisplayUsingInfo();//显示谁在游戏等信息
        this.PlayBgMusic();//播放背景音乐
        this.ShowRecoveryHP();//显示恢复一滴血倒计时
    }

    public Close() {
        egret.Tween.removeTweens(this.mLineParent);
        this.mLineParent.rotation = 0;
        this.StopBgMusic();
        this.StopBarrage();
        this.mOnlineUserAvatarArray = [];
        //该方法用于退出当前的频道。退出成功后，频道内的所有其他用户将收到 onChannelUserLeaved 回调。
        // this.mChannel.channelLeave(() => { });
        // this.agoraSession.logout();
        this.StopHitEffect();
        this.ResetShootAvatarInfo();
        super.Close();
    }
//初始化游戏信息 开始绳子动画
    private Init() {
        this.mbGuide = true;
        this.mTimeEnterFrame = egret.getTimer();
        this.mbShoot = false;  //服务器通过  客户端也通过  则认为可中奖
        this.mBulletTweening = false;
        this.mBullet.visible = false;
        this.mBulletDelayTickCount = 0;
        this.mBulletTargetPosY = 0;
        this.mShootAvatarInitPosY = this.mShootAvatar.y;
        this.mScroller.bounces = false;
        this.mScroller.viewport.scrollV = 0;

        this.mID = PlayerData.GetInstance().GetID().toString();
        this.mName = PlayerData.GetInstance().GetName();
        this.mAvatar = PlayerData.GetInstance().GetAvater();

        this.mChatTextFlow = [];
        let dataObj = this.GetData("shootDollData");
        this.mShootDollData = new ShootDollData( dataObj);
        this.mDeviceID = this.mShootDollData.mDeviecID;
        this.mRemoteUserID = this.mDeviceID;
        this.mChannel = this.mDeviceID;
        this.mShootNum.text = "已射中" + this.mShootDollData.mNum + "次";

        //let signal = Signal("b4fdddb559d241e58c9adad9ef266cb4");
        //this.agoraSession = signal.login(PlayerData.GetInstance().GetID(), PlayerData.GetInstance().mSignalingKey);
        // this.agoraSession.onLoginSuccess = (data) => {
        //     this.agoraSession.invoke("io.agora.signal.user_set_attr", { "name": "icon", "value": this.mAvatar }, (err, val) => {
        //         this.DoJoin(this.mDeviceID);
        //     });
        // }
        // this.agoraSession.onLoginFailed = (ecode) => { Toast.launch("登录失败" + ecode); }
        // this.agoraSession.onError = (evt) => { Toast.launch("登录错误" + evt) }
        // this.agoraSession.onMessageInstantReceive = (account, uid, data) => { };
        //绳子动画
        this.NewTweenLine();
    }

    public InitBarrage() {
        //RoomBarrageManager.GetInstance().Init(this);
    }

    public StopBarrage() {
       // RoomBarrageManager.GetInstance().CloseBarrage();
    }
    //绳子动画
    public NewTweenLine() {
        let iTween = egret.Tween.get(this.mLineParent, { "loop": true });
        iTween.to({ "rotation": 5 }, 1500).to({ "rotation": -5 }, 3000).to({ "rotation": 0 }, 1500);
    }
    //圆环  子弹 动画
    private EnterFrame() {
        let now = egret.getTimer();
        let pass = now - this.mTimeEnterFrame;
        this.mTimeEnterFrame = egret.getTimer();
        this.Update(pass);
    }

    private Update(deltaTime: number) {
        // egret.log(deltaTime);
        //this.TweenLine(deltaTime);
        this.UpdateCircleAngle(deltaTime);
        this.UpdateBulletInfo(deltaTime);
    }
    //Discarded
    private StartTick(time: number): boolean {
        // egret.log("StartTick" + time);
        //this.TweenLine(time);
        this.UpdateCircleAngle(time);
        this.UpdateBulletInfo(time);
        return true;
    }
    //初始化子弹数据和旋转的环 距离   角度设定等信息
    private InitRotateInfo() {
        // this.mCircleAddAngleRate = Helper.Random(5, 15) / 5;
        this.mCircleAddAngle = this.mCircleDefaultAddAngle;
        this.mCircleAddAngle *= this.mCircleAddAngleRate;
        this.mBulletAddPosY = this.mBulletDefaultAddPosY;
        this.mBulletStartY = this.height - this.mBulletStartPoint.bottom;
        this.mBulletStartX = this.mBulletStartPoint.x;
        this.mBulletMoveDistanceForCalculation = this.mBulletStartY - this.mRotateCircle.y - this.mRotateCircle.height * .5;
        this.mThroughCircleMinAngle = 210;
        this.mThroughCircleMaxAngle = 270;
    }
//显示血量信息
    private InitMonsterHpInfo() {
        //初始化当前剩余血量
      

        this.mMonsterTotalHp = this.mShootDollData.mHp;
        this.mMonsterCurHp = this.mShootDollData.mHp;
        let data: Array<Object> = [];
        for (let i = 0; i < this.mMonsterTotalHp; i++) {
            let bEmpty: boolean = this.mMonsterCurHp <= i;
            data.push({ empty: bEmpty });
        }
        this.mHpCollection = new eui.ArrayCollection(data);
        let gap = 0;
        let horizontalLayout = this.mHpList.layout as eui.HorizontalLayout;
        if (horizontalLayout) gap = horizontalLayout.gap;

        this.mHpList.width = this.mMonsterTotalHp * 40 + (this.mMonsterTotalHp - 1) * gap;
        this.mHpList.dataProvider = this.mHpCollection;
        this.mHpList.itemRenderer = MonsterHpIRUI;
    }
    //获取当前用户 当前娃娃的剩余血量
    private getCurMonsterHp(){
          let params = {id:this.mShootDollData.mDollID};
		HttpManager.SendPost(HttpManager.ENTER_SHOOL_DOLL, this, params, (data) => {
			let code = data.code;
			if (code === 200) {
				let dollData = data['data'];
				this.mMonsterCurHp = this.mMonsterTotalHp - dollData.current;
                this.DisplayMonsterHp();
			} else {
				Toast.launch(data.descrp);
			}

		});
    }
//显示要射击的图片
    private DisplayShootInfo() {
        Helper.SetImageCrossOrigin(this.mShootAvatar, this.mShootDollData.mShootImg);
        // Helper.ImageAddMask(this.mShootAvatar);
    }

    private DisplayMonsterHp() {
        let data: Array<Object> = [];
        for (let i = 0; i < this.mMonsterTotalHp; i++) {
            let bEmpty: boolean = this.mMonsterCurHp <= i;
            data.push({ empty: bEmpty });
        }
        this.mHpCollection.replaceAll(data);
    }

    private UpdateCircleAngle(deltaTime: number) {
        this.mCircleCurAngle += this.mCircleAddAngle * deltaTime;
        //(0, 180) (-180, 0)
        if (this.mCircleCurAngle >= 180) {
            this.mCircleCurAngle -= 360;
        }
        this.mRotateCircle.rotation = this.mCircleCurAngle;
    }

    private UpdateBulletInfo(deltaTime: number) {
        if (this.mBulletTargetPosY <= 0) return;
        if (this.mBulletDelayTickCount > 0) {
            this.mBulletDelayTickCount -= deltaTime;
            return;
        }
        if (!this.mBullet.visible) this.mBullet.visible = true;

        let curAngle = this.mBulletStar.rotation;
        curAngle += this.mBulletAddAngle * deltaTime;
        if (curAngle >= 180) {
            curAngle -= 360;
        }
        this.mBulletStar.rotation = curAngle;

        if (this.mBulletTweening) return;

        this.mBullet.y -= this.mBulletAddPosY * deltaTime;
        if (this.mBullet.y <= this.mBulletTargetPosY) {
            this.mBullet.y = this.mBulletTargetPosY;
            if (this.mbShoot) {
                this.mBulletTargetPosY = 0;
                this.mBullet.visible = false;
                this.StopBulletDragEffect();
                this.PlayHitEffect();
                this.PlayHitSoundEffect();
                this.PlayWinSoundEffect();
                this.mMonsterCurHp--;
                this.DisplayMonsterHp();
                if (this.mMonsterCurHp <= 0) {//最后射中  血量全射中
                    this.TweenShootAvatar(() => {
                        this.ShootDollSuccess();
                        this.RefreshShootDollInfo();
                        this.ResetShootAvatarInfo();
                        this.reportGameResult("success");
                    });
                } else {//射中 但是还有血量
                    this.reportGameResult("success");
                    let msg = this.GetShootBarrage(this.mName);//获取射中的描述字符串
                    this.DisplayRoomBarrage(this.mAvatar, msg, this.mName);
                    this.PlayeEffect();
                }
                this.mbShoot = false;
            } else {
                let msg = this.GetNotShootBarrage(this.mName);//获取没有射中的描述字符串
                //this.DisplayRoomBarrage(this.mAvatar, msg, this.mName);
                this.PlayHitMissSoundEffect();
                egret.Tween.removeTweens(this.mBullet);
                let bRight = Helper.Random(0, 2) == 1;//随机  让子弹星星  偏左 还是偏右
                let addX = Helper.Random(100, 200);
                let targetX = this.mBullet.x + (bRight ? addX : -addX);
                let targetY = this.mBullet.y + Helper.Random(100, 200);
                this.mBulletTweening = true;
                egret.Tween.get(this.mBullet).to({ x: targetX, y: targetY }, 400, egret.Ease.sineIn).call(() => {
                    this.mBulletTargetPosY = 0;
                    this.mBulletTweening = false;
                    this.mBullet.visible = false;
                    this.StopBulletDragEffect();
                });
                this.reportGameResult("fail");
            }

            //this.GiftTween();//显示再射几次 获取精美礼品
            this.mBulletAddPosY = this.mBulletDefaultAddPosY;
            this.mCircleAddAngle = this.mCircleDefaultAddAngle;
            this.mCircleAddAngle *= this.mCircleAddAngleRate;
        }
    }

    private PlayHitEffect() {
        if (!this.mHitEffectPoint)
            this.mHitEffectPoint = ParticleManager.CreateParticle(this.mRotateCircle.parent, this.mRotateCircle.x, this.mRotateCircle.y, "hit_effect_dian_png", "hit_effect_dian_json");
        if (!this.mHitEffectStar)
            this.mHitEffectStar = ParticleManager.CreateParticle(this.mRotateCircle.parent, this.mRotateCircle.x, this.mRotateCircle.y, "hit_effect_star_png", "hit_effect_star_json");
        this.mHitEffectPoint.start();
        this.mHitEffectStar.start();
        this.mHitSetTimeOut = setTimeout(() => this.StopHitEffect(), 500);
    }

    private StopHitEffect() {
        if (this.mHitSetTimeOut) clearTimeout(this.mHitSetTimeOut);
        if (this.mHitEffectPoint) this.mHitEffectPoint.stop();
        if (this.mHitEffectStar) this.mHitEffectStar.stop();
    }
//，服务器不让中，则修改角度
    private RotateChange() {
        let curRotation = this.mRotateCircle.rotation;
        if (curRotation < 0) curRotation += 360;
        let bulletCurTickCount = this.mBulletMoveDistanceForCalculation / this.mBulletAddPosY;
        let virtualRealRotation = curRotation + this.mCircleAddAngle * bulletCurTickCount;
        let realRotation = virtualRealRotation % 360;
        let bThroughCircle: boolean = realRotation >= this.mThroughCircleMinAngle && realRotation <= this.mThroughCircleMaxAngle;
        //如果本身能中  ，服务器不让中，则修改角度
        if (this.mbShoot != bThroughCircle) {
            let targetRotation = realRotation;
            let space = Math.abs(this.mThroughCircleMaxAngle - this.mThroughCircleMinAngle);
            if (this.mbShoot) {
                targetRotation = Helper.Random(this.mThroughCircleMinAngle + space * .25, this.mThroughCircleMaxAngle - space * .25);
            } else {
                if (Math.abs(this.mThroughCircleMaxAngle - realRotation) < Math.abs(this.mThroughCircleMinAngle - realRotation)) {
                    targetRotation = Helper.Random(this.mThroughCircleMaxAngle, this.mThroughCircleMaxAngle + space * .25);
                } else {
                    targetRotation = Helper.Random(this.mThroughCircleMinAngle, this.mThroughCircleMinAngle - space * .25);
                }
            }
            let addRotation = targetRotation - realRotation;
            if (addRotation > 180) {
                addRotation -= 360;
            }
            this.mBulletDelayTickCount = 0;
            let bulletExtraAddPos = 0;
            let circleExtraAddAngle = 0;
            let bNeedAdjustBulletExtraAddPos = true;
            if (addRotation > 0) {
                this.mBulletDelayTickCount = Math.min(this.mBulletDelayMaxTickCount, addRotation / this.mCircleAddAngle);
                let extraAddRotation = addRotation - this.mBulletDelayTickCount * this.mCircleAddAngle;;

                if (extraAddRotation <= 0) {
                    circleExtraAddAngle = extraAddRotation / (bulletCurTickCount + this.mBulletDelayTickCount);
                    bNeedAdjustBulletExtraAddPos = false;
                }
            }
            if (bNeedAdjustBulletExtraAddPos) {
                circleExtraAddAngle = addRotation / (bulletCurTickCount + this.mBulletDelayTickCount);
                let index = 1;
                while (true) {
                    if (index > 6) break;

                    if (index & 1) {
                        bulletExtraAddPos = this.GetBulletExtraAddAngle(circleExtraAddAngle * .5, addRotation);
                    } else {
                        circleExtraAddAngle = this.GetCircleExtraAddAngle(bulletExtraAddPos * .5, addRotation);
                    }

                    index++;
                }
            }
            this.mCircleAddAngle += circleExtraAddAngle;
            this.mBulletAddPosY += bulletExtraAddPos;
            // console.log(`v=${this.mBulletAddPosY}; x=${this.mCircleAddAngle}; s=${this.mBulletMoveDistanceForCalculation}; B=${curRotation}; y0=${realRotation}; t0=${this.mBulletdelayTickCount / 60}; y1=${targetRotation}; angle=${addRotation}`);
        }
        this.mRotationDes.text = this.mbShoot.toString();
    }

    //y:需要调整的角度； s：子弹移动的距离； x：环的转速；x0：环的默认转速
    //v:子弹速度；v0：子弹默认速度；t0：子弹发射延迟时间
    //y = x0 * t0 + (x / v - x0 / v0) * s;
    /**
     * 获取额外的环的加速 
     * x = v * ((y - x0 * t0) / s + x0 / v0)
    */
    private GetCircleExtraAddAngle(bulletExtraAddPos: number, addRotation: number): number {
        let v = this.mBulletAddPosY + bulletExtraAddPos;
        let y = addRotation;
        let x0 = this.mCircleAddAngle;
        let t0 = this.mBulletDelayTickCount;
        let s = this.mBulletMoveDistanceForCalculation;
        let v0 = this.mBulletAddPosY;
        let x = v * ((y - x0 * t0) / s + x0 / v0);
        return x - x0;
    }

    /**
     * 获取额外的子弹的加速 
     * v = x / ((y - x0 * t0) / s + x0 / v0)
    */
    private GetBulletExtraAddAngle(circleExtraAddAngle: number, addRotation: number): number {
        let x = this.mCircleAddAngle + circleExtraAddAngle;
        let y = addRotation;
        let x0 = this.mCircleAddAngle;
        let t0 = this.mBulletDelayTickCount;
        let s = this.mBulletMoveDistanceForCalculation;
        let v0 = this.mBulletAddPosY;
        let v = x / ((y - x0 * t0) / s + x0 / v0);
        return v - v0;
    }
//判断是否可中
    private CanThroughByDefault(): boolean {
        let curRotation = this.mRotateCircle.rotation;
        if (curRotation < 0) curRotation += 360;

        let bulletCurTickCount = this.mBulletMoveDistanceForCalculation / this.mBulletAddPosY;
        let virtualRealRotation = curRotation + this.mCircleAddAngle * bulletCurTickCount;
        let realRotation = virtualRealRotation % 360;
        let bThroughCircle: boolean = false;
        let offsetRotation = 0;
        if (this.mThroughCircleMinAngle <= realRotation + offsetRotation && realRotation + offsetRotation <= this.mThroughCircleMaxAngle) {
            bThroughCircle = true;
        }
        return bThroughCircle;
    }

    private ShootBullet() {
        this.mBullet.x = this.mBulletStartX;
        this.mBullet.y = this.mBulletStartY;
        this.mBulletTargetPosY = this.mRotateCircle.y;
        if (!this.mbShoot) {
            this.mBulletTargetPosY += this.mRotateCircle.height * .5;
        }
    }

    private PlayBulletDragEffect() {
        if (!this.mBulletDragEffect) {
            this.mBulletDragEffect = ParticleManager.CreateParticle(this.mBullet, this.mBullet.width * .5, this.mBullet.height * .5, "shoot_drag_png", "shoot_drag_json");
        }
        this.mBulletDragEffect.start();
    }

    private StopBulletDragEffect() {
        if (this.mBulletDragEffect) {
            this.mBulletDragEffect.stop();
        }
    }
    //开始射击
    private StartShoot(serverData) {
        if (this.mBulletTargetPosY > 0) return;
        if (!this.HaveEnoughCost()) {
            UIManager.OpenUI(UIName.CoinNotEnoughUI, this.parent);
            return;
        }
        //this.ReduceCost();
        this.DisplayDiamondNum();
        //this.DisplayCostNum();

        //服务器认为可以射中，并且客户端打中时 this.mbShoot = true;
        let bCanShoot = serverData.predict_result == '1';
        egret.log("==="+serverData.predict_result);
        let bCanThrough = this.CanThroughByDefault();
        this.mbShoot = bCanShoot && bCanThrough;//最后判断能否中奖

        this.mRotationDes.text = `${this.mbShoot}`;//暂未发现用途
        this.ShootBullet();
        this.PlayBulletDragEffect();
        this.RotateChange();//
        this.TellAndCheckServerShootStatus();
    }

    private HaveEnoughCost(): boolean {
        let bEnough = true;
        //let totalticket: number = PlayerData.GetInstance().Getticketnum();
        // let ticket = this.mShootDollData.mTicket;
        // if (ticket > 0 && totalticket >= ticket) {
        //     bEnough = true;
        // }
        // let totalDiamond = PlayerData.GetInstance().GetDiamond();
        // let price = this.mShootDollData.mPrice;
        // if (price > 0 && totalDiamond >= price) {
        //     bEnough = true;;
        // }
        return bEnough;
    }

    private requestBeginGame() {
        //let totalticket: number = PlayerData.GetInstance().Getticketnum();
        // let ticket = this.mShootDollData.mTicket;
        // if (ticket > 0 && totalticket > 0 && totalticket >= ticket) {
        //     PlayerData.GetInstance().Setticketnum(totalticket - ticket);
        // } else {
        //     PlayerData.GetInstance().AddDiamond(-this.mShootDollData.mPrice);
        // }
        let params = {game_type:"1",id:this.mShootDollData.mDollID};
		HttpManager.SendPost(HttpManager.BEIGIN_GAME, this, params, (data) => {
			let code = data.code;
			if (code === 200) {
				let temp = data['data'];
                this.game_log_id = temp.game_log_id;
				PlayerData.GetInstance().SetDiamond(temp.balance);
                this.StartShoot(temp);
			} else {
				Toast.launch(data.descrp);
			}

		});
        
    }

    private TellAndCheckServerShootStatus() {
        // let hit = "0";
        // if (this.mbShoot) hit = "1";
        // let params = { doll_id: this.mShootDollData.mDollID, tvmid: HttpManager.msTvmId, hit: hit };
        // HttpManager.SendPost(HttpManager.TVM_SHOOT_DOLL, this, params, (data) => {
        //     let code = data.code;
        //     if (code === 200) {
        //         let shoot = data.shoot_ret;
        //         //客户端自己减hp
        //         // let shootInfo = data.shoot_info;
        //         // if(shootInfo){
        //         //     let curHp = shootInfo.blood;
        //         //     this.mMonsterCurHp = parseInt(curHp);
        //         //     this.DisplayMonsterHp();
        //         // }
        //         egret.log(data.pointadd);
        //         if (data.pointadd != 0) {
        //             let msg = "元宝+" + data.pointadd;
        //             Toast.Underlaunch(msg, 50);
        //         }
        //     } else {
        //         // Toast.launch(data.descrp);
        //     }
        // });
    }
    //显示射中
    private ShootDollSuccess() {
        //let msg = UICommon.GetShootSuccessBarrage(this.mName, this.mShootDollData.mName);
        // this.DisplayRoomBarrage(this.mAvatar, msg, this.mName);
        UIManager.OpenUI(UIName.ShootSuccessUI, this.parent, (openUI: BaseUI) => {
            if (openUI) {
                openUI.SetData("imageURL", this.mShootDollData.mImg);
            }
        });

        //WorldBarrageSocket.GetInstance().EmitShoot(this.mAvatar, this.mName, msg, this.mShootDollData.mDollID, this.mShootDollData.mName);
    }
    //中奖后
    private TweenShootAvatar(callBack: Function) {
        egret.Tween.removeTweens(this.mShootAvatar);
        egret.Tween.get(this.mShootAvatar).to({ y: 650 }, 1000).call(() => {
            if (callBack) callBack();
        });
    }

    private ResetShootAvatarInfo() {
        egret.Tween.removeTweens(this.mShootAvatar);
        this.mShootAvatar.y = this.mShootAvatarInitPosY;
    }

    private reportGameResult(result:string){
        if(Helper.StrEmptyOrNull(this.game_log_id))return;
        let params = {game_type:"1",game_result:result,game_log_id:this.game_log_id};
		HttpManager.SendPost(HttpManager.REPORT_GAME_RESULT, this, params, (data) => {
			let code = data.code;
			if (code === 200) {
				
			} else {
				//Toast.launch(data.descrp);
			}

		});
        this.game_log_id = "";

    }

    /**成功后 刷新新的娃娃 */
    private RefreshShootDollInfo() {
        // let params = { doll_id: this.mShootDollData.mDollID, uid: PlayerData.GetInstance().GetID() };
        // HttpManager.SendPost(HttpManager.GET_SHOOT_CAN_HITED, this, params, (data) => {
        //     let code = data.code;
        //     if (code === 200) {
        //         this.mShootDollData.mHp = this.mShootDollData.mMaxHp;
        //         this.InitMonsterHpInfo();
        //         this.DisplayMonsterHp();
        //     } else {
        //         Toast.launch(data.descrp);
        //     }
        // });
        this.InitMonsterHpInfo();
        this.DisplayMonsterHp();
    }

    private VisibleGuide() {
        this.mbGuide = GameData.GetInstance().GetShootGuide();
        this.mGuideGroup.visible = !this.mbGuide;
        if (!this.mbGuide) GameData.GetInstance().SetShootGuide();
    }
//播放背景音乐
    public PlayBgMusic() {
        SoundManager.PlaySoundMusic("shootBgMusic.mp3");
    }

    public StopBgMusic() {
        SoundManager.StopSoundMusic("shootBgMusic.mp3");
    }

    private PlayClickSoundEffect() {
        SoundManager.PlaySoundEffect("shootClick.mp3", 0, 1);
    }

    private PlayHitSoundEffect() {
        SoundManager.PlaySoundEffect("shoot.mp3", 0, 1);
    }

    private PlayHitMissSoundEffect() {
        SoundManager.PlaySoundEffect("shootMiss.mp3", 0, 1);
    }

    private PlayWinSoundEffect() {
        SoundManager.PlaySoundEffect("shootWin.mp3", 0, 1);
    }

    public DoJoin(name: any) {
        this.mChannel = this.agoraSession.channelJoin(name);
        this.mChannel.onChannelJoined = () => {
            this.agoraSession.invoke("io.agora.signal.channel_query_userlist_all", { "name": this.mDeviceID }, (err, vallist) => {
                let name = this.HandleName(this.mName);
                this.EmitBarrage(this.mAvatar, `【${name}】进入房间`);

                this.SetOnlineNum(parseInt(vallist.num));
                let max = Math.min(5, vallist.list.length);
                let index = 0;
                for (let i = 0; i < max; i++) {
                    let id = vallist.list[i][0];
                    this.GetAudiencesAvatar(id, (avatar) => {
                        index++;
                        if (avatar) {
                            this.PushOnlineUserArray(id, avatar);
                        } else {
                            //没有头像，减去
                            this.AddOnlineNum(-1);
                        }

                        if (index == max)
                            this.DisplayAudienceAvatar();
                    });
                }
            });
        };

        this.mChannel.onChannelUserLeaved = (data) => {
            this.GetAudiencesAvatar(data, (avatar) => {
                this.AddOnlineNum(-1);
                this.RemoveOnlineUserArray(data, avatar);
                this.DisplayAudienceAvatar();
            });
        }
        this.mChannel.onChannelUserJoined = (data) => {
            this.GetAudiencesAvatar(data, (avatar) => {
                this.AddOnlineNum(+1);
                this.PushOnlineUserArray(data, avatar);
                this.DisplayAudienceAvatar();
            });
        }

        this.mChannel.onChannelJoinFailed = () => {
            Toast.launch("加入信令失败，请退出重试");
        };

        //频道消息回调
        this.mChannel.onMessageChannelReceive = (account, uid, data) => {
            var data = JSON.parse(data);
            switch (data.messageType) {
                case this.NORMAL:
                    this.OnChat(data);
                    break;
            }
        }
    }
//获取在线头像
    private GetAudiencesAvatar(id: string, callBack: Function) {
        this.agoraSession.invoke("io.agora.signal.user_get_attr_all", { "account": id }, (err, val) => {
            let id = val.account;
            let avatar = JSON.parse(val.json).icon;
            if (callBack) {
                callBack(avatar);
            }
        });
    }

    ///发送频道消息
    private EmitNewMessage(message: string, chatType?: string) {
        let msg = JSON.stringify(
            {
                uid: this.mID,
                remoteUid: this.mRemoteUserID,
                messageContent: message,
                userName: this.mName,
                playerAvatar: this.mAvatar,
                userAvatar: this.mAvatar,
                messageType: this.NORMAL,
                chatType: chatType
            });

        this.mChannel.messageChannelSend(msg, () => {
        })
    }

    //聊天
    private EmitChat(msg: string) {
        //WorldBarrageSocket.GetInstance().Emit(this.mAvatar, this.mName, msg, this.mDeviceID, 'chat', 'shoot');
    }

    ///弹幕
    private EmitBarrage(avatarURL: string, msg: string, ) {
        this.EmitNewMessage(msg, "barrage");
    }
//获取并显示奖品详情
    private RequestDollDetailInfo() {
        //this.mDollDetailData = new DollDetailData();
        // HttpManager.SendPost(HttpManager.GET_DOLL_DETAIL_INFO, this, { dollid: this.mShootDollData.mDollID }, (data) => {
        //     //无错误码
        //     this.mDollDetailData.mAvatarURL = this.mShootDollData.mImg;
        //     this.mDollDetailData.mName = data.name;
        //     this.mDollDetailData.mLength = data.length;
        //     this.mDollDetailData.mDes = data.des;
        //     this.mDollDetailData.mTransportInfo = data.transportInfo;

        //     this.DisplayDollDetailInfo();
        // });
        this.DisplayDollDetailInfo();
    }

    private AddOnlineNum(addNum: number) {
        this.SetOnlineNum(this.mOnlineNum + addNum);
    }

    private SetOnlineNum(num: number) {
        num = Math.max(0, num);
        this.mOnlineNum = num;
        this.mOnlineNumLabel.text = `${this.mOnlineNum}人`;
    }

    private PushOnlineUserArray(id: string, avatar: string) {
        this.mOnlineUserAvatarArray.push([id, avatar]);
    }

    private RemoveOnlineUserArray(id: string, avatar: string) {
        let index = this.mOnlineUserAvatarArray.indexOf([id, avatar]);
        if (index > -1) this.mOnlineUserAvatarArray.splice(index, 1);
    }
//初始化观众信息  在线人数 和前三位
    private InitAudienceAvatar() {
        this.mAudienceAvatar.mask = this.mAudienceAvatarMask;
        this.mAudienceAvatar0.mask = this.mAudienceAvatarMask0;
        this.mAudienceAvatar1.mask = this.mAudienceAvatarMask1;
    }

    private DisplayAudienceAvatar() {
        let avatars: Array<string> = [];
        for (let v of this.mOnlineUserAvatarArray) {
            if (v[1]) avatars.push(v[1]);
            if (avatars.length >= 3) break;
        }

        if (avatars[0]) Helper.SetImageCrossOrigin(this.mAudienceAvatar, avatars[0]);
        if (avatars[1]) Helper.SetImageCrossOrigin(this.mAudienceAvatar0, avatars[1]);
        if (avatars[2]) Helper.SetImageCrossOrigin(this.mAudienceAvatar1, avatars[2]);

        let lenght = avatars.length;
        this.mAudiencesInfo.right = -160 + lenght * 50;
    }
//显示是否收藏
    private DisplayStarStatus() {
        //this.mStarToggle.selected = PlayerData.GetInstance().IsStar(this.mDeviceID);
    }
//显示用户余额等信息  是金币还是娃娃券
    private DisplayDiamondNum() {
        let totalDiamond: number = PlayerData.GetInstance().GetDiamond();
        let strDiamond: string = totalDiamond > 10000 ? `${totalDiamond / 10000}万` : totalDiamond.toString();
        this.mDiamondNum.text = `当前拥有:${strDiamond}`;
        this.mDiamondIcon.x = this.mDiamondNum.textWidth + 10;
    }
//显示需要花费多少金币 或者券
    private DisplayCostNum() {
        //let totalticket: number = PlayerData.GetInstance().Getticketnum();
        // let ticket = this.mShootDollData.mTicket;
        // if (ticket > 0 && totalticket > 0 && totalticket >= ticket) {
        //     this.mCostNum.text = `${ticket}`;
        // } else {
        //     this.mCostNum.text = `${this.mShootDollData.mPrice}`;
        // }

        this.mCostNum.text = `${this.mShootDollData.mPrice}`;
    }
//显示谁在游戏等信息
    private DisplayUsingInfo() {
        this.mUsingDes.text = `${this.HandleName(this.mName)}正在游戏中`;
        Helper.SetImageCrossOrigin(this.mUserHead, this.mAvatar);
        this.mUserHead.mask = this.mUserHeadMask;
    }

    private OnChat(data) {
        // egret.log("OnChat: " + data);
        let type = data.chatType;
        let msg = data.messageContent;
        let name = data.userName;
        if (type === "chat") {
            this.AddChatContent(name, msg);
            this.RefreshChatDisplayedInfo();
        } else if (type === "barrage") {
            let avatarURL = data.userAvatar;
            this.DisplayRoomBarrage(avatarURL, msg, name);
        }
    }

    /**world chat */
    public WorldOnChat(name: string, msg: string) {
        this.AddChatContent(name, msg);
        this.RefreshChatDisplayedInfo();
    }

    private AddChatContent(name: string, content: string) {
        name = this.HandleName(name);
        this.mChatTextFlow.push({ text: (this.mChatTextFlow.length > 0 ? '\n' : '') + `${name}: `, style: { "textColor": 0xffff70 } });
        this.mChatTextFlow.push({ text: `${content}`, style: { "textColor": 0xffffff } });

        if (this.mChatTextFlow.length > this.mChatMaxCount * 2) {
            this.mChatTextFlow.shift();
            this.mChatTextFlow.shift();
        }
    }
//聊天信息显示收藏控制
    private RefreshChatDisplayedInfo() {
        let bVisible = !this.mChatVisibleToggle.selected;
        this.mChatContent.visible = bVisible;
        this.mChatContentBg.visible = bVisible;
        if (!bVisible) return;

        this.mChatContent.textFlow = this.mChatTextFlow;
        this.mChatContentBg.height = Math.min(this.mChatScroller.height, this.mChatContent.textHeight) + 40;
    }
//聊天助手显示隐藏
    private VisibleChatHandledInfo(visible: boolean) {
        this.mChatHandledInfo.visible = visible;
    }

    private InitChatPhraseInfo() {
        let dataArray: Array<Object> = [];
        dataArray.push({ text: "666" });
        dataArray.push({ text: "2333" });
        dataArray.push({ text: "厉害了我的哥" });
        dataArray.push({ text: "你好菜呀" });
        dataArray.push({ text: "放开那个娃娃，让我来" });
        dataArray.push({ text: "扎心了，老铁" });
        this.mChatPhraseList.dataProvider = new eui.ArrayCollection(dataArray);
        this.mChatPhraseList.itemRendererSkinName = "CharPhraseIRSkin";
        this.mChatPhraseList.addEventListener(eui.ItemTapEvent.ITEM_TAP, this.ClickChatPhraseItem, this);
    }

    private ClickChatPhraseItem(evt: eui.ItemTapEvent) {
        let msg = this.mChatPhraseList.selectedItem.text;
        this.EmitChat(msg);
        this.VisibleChatHandledInfo(false);
    }

    private VisibleChatPhraseInfo() {
        this.mChatPhraseInfo.visible = !this.mChatPhraseVisibleToggle.selected;
    }

    private GetShootBarrage(name: string): string {
        name = this.HandleName(name);

        let barrage = "";
        let arry: Array<string> = [];
        arry.push(`【${name}】射中守护兽啦，加油，胜利在望`);
        arry.push(`守护兽：为什么【${name}】你要打我`);

        let randomNum = Helper.Random(0, arry.length);
        return arry[randomNum];
    }

    private GetNotShootBarrage(name: string): string {
        name = this.HandleName(name);

        let barrage = "";
        let arry: Array<string> = [];
        arry.push(`真可惜，【${name}】差点就射中了~`);
        arry.push(`我勒个去，【${name}】这样都没射中~`);

        let randomNum = Helper.Random(0, arry.length);
        return arry[randomNum];
    }

    private HandleName(name: string): string {
        return UICommon.HandleName(name);
    }

    private DisplayRoomBarrage(avatarURL: string, msg: string, name: string) {
       // RoomBarrageManager.GetInstance().Lanuch(avatarURL, msg, name);
    }
//显示空闲   同类型机器
    private DisplaySameInfo(sameData = null) {
        // let freeCount = 0;
        // let totalCount = 0;
        // let data: Array<Object> = [];

        // if (sameData) {
        //     let freeStatusType = 2;
        //     for (let v of sameData) {
        //         let status = v.channel_status;
        //         if (status == freeStatusType) {
        //             freeCount++;
        //         }
        //         totalCount++;

        //         //data.push({ id: v.id, icon: this.mShootDollData.mImg, status: v.channel_status });
        //     }
        // }

        // this.mSameFreeCount.text = freeCount.toString();
        // this.mSameTotalCount.text = totalCount.toString();
        // this.mSameList.dataProvider = new eui.ArrayCollection(data);
       // this.mSameList.itemRenderer = SameItemIRUI;
    }

    private ClickSameItem(item: eui.ItemTapEvent) {
        //let id = this.mSameList.selectedItem.id;
       // UICommon.AutoOpenVideoDisplayUIByDeviceID(id, this);
    }

    private SameBtn(evt: egret.TouchEvent) {
        // let min = 0;
        // let max = this.mSameScroller.viewport.contentWidth - this.mSameScroller.width;
        // if (max <= min) return;
        // let target = evt.currentTarget as eui.Button;
        // let moveDistance = -110;
        // if (target === this.mSameLeftBtn) {
        //     moveDistance = 110;
        // }

        // let scrollH = this.mSameScroller.viewport.scrollH;
        // scrollH = Math.min(max, Math.max(min, scrollH + moveDistance));
        // this.mSameScroller.viewport.scrollH = scrollH;
    }
//显示娃娃详情 点击切换 房间记录
    private VisibleDollDetailInfo(visible: boolean) {
        this.mDollDetailGroup.visible = visible;
    }
//显示娃娃详情 
    private DisplayDollDetailInfo() {
        Helper.SetImageCrossOrigin(this.mDollAvatar, this.mShootDollData.mImg);
        //Helper.ImageAddMask(this.mDollAvatar);
        this.mDollName.text = `名称：${this.mShootDollData.mName}`;
        this.mDollDes.text = `描述：${this.mShootDollData.mDes}`;
        this.mDollTransportInfo.text = `发货方式：${this.mShootDollData.mTransportInfo}`;
    }
//房间记录
    private VisibleRecordGroup(visible: boolean) {
        this.mRecordGroup.visible = visible;
    }

    private RequestRecords() {
        this.mRecordList.dataProvider = null;
        // let params = { doll_id: this.mShootDollData.mDollID };
        // HttpManager.SendPost(HttpManager.GET_SHOOT_DOLL_RECORD, this, params, (data) => {
        //     let code = data.code;
        //     if (code === 200) {
        //         let dataList: Array<Object> = [];
        //         for (let v of data.doll_data) {
        //             let userName = v.user_login;
        //             dataList.push({ icon: v.avatar, name: userName, time: v.shoot_time });
        //         }
        //         this.mRecordList.dataProvider = new eui.ArrayCollection(dataList);
        //         this.mRecordList.itemRenderer = RecordUserListItem;
        //     }
        //     else {
        //         Toast.launch(data.descrp);
        //     }
        // });
    }

    private PlayeEffect() {
        if (!this.particle2) this.particle2 = ParticleManager.CreateParticle(this, 318, 0, "cheng_png", "cheng_json");
        if (!this.particle3) this.particle3 = ParticleManager.CreateParticle(this, 318, 50, "dian_png", "dian_json");
        if (!this.particle4) this.particle4 = ParticleManager.CreateParticle(this, 318, 0, "hong_png", "hong_json");
        if (!this.particle5) this.particle5 = ParticleManager.CreateParticle(this, 318, 0, "huang_png", "huang_json");
        if (!this.particle6) this.particle6 = ParticleManager.CreateParticle(this, 318, 0, "lan_png", "lan_json");
        if (!this.particle7) this.particle7 = ParticleManager.CreateParticle(this, 318, 0, "lv_png", "lv_json");
        if (!this.particle8) this.particle8 = ParticleManager.CreateParticle(this, 318, 0, "qing_png", "qing_json");
        if (!this.particle9) this.particle9 = ParticleManager.CreateParticle(this, 318, 50, "xx_png", "xx_json");
        if (!this.particle10) this.particle10 = ParticleManager.CreateParticle(this, 318, 0, "zi_png", "zi_json");

        this.particle2.start(1000);
        this.particle3.start(1000);
        this.particle4.start(1000);
        this.particle5.start(1000);
        this.particle6.start(1000);
        this.particle7.start(1000);
        this.particle8.start(1000);
        this.particle9.start(1000);
        this.particle10.start(1000);
    }

    private StopPlayEffect() {
        if (this.particle2) this.particle2.stop();
        if (this.particle3) this.particle3.stop();
        if (this.particle4) this.particle4.stop();
        if (this.particle5) this.particle5.stop();
        if (this.particle6) this.particle6.stop();
        if (this.particle7) this.particle7.stop();
        if (this.particle8) this.particle8.stop();
        if (this.particle9) this.particle9.stop();
        if (this.particle10) this.particle10.stop();
    }

    //=============================button method====================================
    protected BackBtn() {
        // HttpManager.RefreshTVMToken(() => {
        //     UIManager.OpenUI(UIName.MainUI, this.parent);
        //     UIManager.OpenUI(UIName.FooterUI, this.parent);
        //     this.StopPlayEffect();
        //     super.BackBtn();
        // });

        this.StopPlayEffect();
        super.BackBtn();
    }

    private ChatBtn() {
        //Toast.launch("123");
        // this.VisibleChatHandledInfo(true);
        // this.mChatInput.text = "";
        // if (!this.mChatPhraseVisibleToggle.selected) {
        //     this.InitChatPhraseInfo();
        // }
        window["shareFriend"]();
    }

    private ChatVisibleToggle() {
        this.RefreshChatDisplayedInfo();
    }

    private ChatSendBtn() {
        let msg = this.mChatInput.text;
        if (Helper.StrEmptyOrNull(msg) || msg === this.mChatInput.prompt) {
            Toast.launch("请输入聊天内容");
            return;
        }

        this.EmitChat(msg);
        this.VisibleChatHandledInfo(false);
    }

    private CloseChatHandleBtn() {
        this.VisibleChatHandledInfo(false);
    }

    private ChatPhraseVisibleToggle() {
        this.VisibleChatPhraseInfo();
    }

    private PayBtn() {
        // if (GameData.GetInstance().IsTVM()) {
        //     window["OpenSystemRecharge"]();
        // } else {
        //     let FooterUI = UIManager.GetUIObj(UIName.FooterUI) as FooterUI;
        //     if (FooterUI) {
        //         FooterUI.ClickFooterRadioByType(eFooterType.Pay);
        //     }
        // }
        window["test"]();
        //Toast.launch("chongzhi");
    }

    private DollDetailBtn() {
        this.VisibleRecordGroup(false);
        this.VisibleDollDetailInfo(true);
    }
    //收藏
    private StarToggle() {
        //let bStar = this.mStarToggle.selected;
        // PlayerData.GetInstance().ModifyStarList(this.mDeviceID.toString(), bStar);
        // let params = { uid: PlayerData.GetInstance().GetID(), setting: PlayerData.GetInstance().GetStrStarList() };
        //HttpManager.SendPost(HttpManager.SET_USER_SETTING, this, params, null);
    }

    //射击按钮
    private ShootBtn() {
        // if (!this.mbGuide) {
        //     this.CloseGuide();
        //     return;
        // }

        this.PlayClickSoundEffect();
        //let params = { doll_id: this.mShootDollData.mDollID, uid: PlayerData.GetInstance().GetID() };
        //let data = {hit:"1"};
        this.requestBeginGame();
        //this.StartShoot(data);
        // HttpManager.SendPost(HttpManager.GET_SHOOT_CAN_HITED, this, params, (data) => {
        //     let code = data.code;
        //     if (code === 200) {
        //         this.StartShoot(data);
        //     } else {
        //         Toast.launch(data.descrp);
        //     }
        // });
    }

    private RecordBtn() {
        this.VisibleRecordGroup(true);
        this.RequestRecords();
        this.VisibleDollDetailInfo(false);
    }

    private OpenShare() {
        UIManager.OpenUI(UIName.SharePhotoUI, this.parent);
    }

    private CloseGuide() {
        this.mGuideGroup.visible = false;
        this.mbGuide = true;
    }
    //显示再射几次  获取精美礼品
    private GiftTween() {
        // let params = { uid: PlayerData.GetInstance().GetID() };
        // HttpManager.SendPost(HttpManager.GET_SHOOT_GIFTTASK_API, this, params, (data) => {
        //     if (data.code == 200) {
        //         PlayerData.GetInstance().shoottaskState = data.taskstate;
        //         PlayerData.GetInstance().shoottaskcurCatchNum = 0;
        //         PlayerData.GetInstance().shoottasktotalCatchNum = 0;
        //         PlayerData.GetInstance().shoottaskTime = 0;
        //         this.priceImage.visible = false;
        //         this.bg.visible = true;
        //         this.bg2.visible = false;
        //         this.itemicon2.visible = true;
        //         this.itemicon.visible = false;
        //         egret.Tween.removeTweens(this.shootGiftGroup);
        //         if (data.taskstate === 0)///0：没任务   1:未完成，进行中  2：已完成可领取   
        //         {

        //             this.shootGiftGroup.visible = false;
        //         } else if (data.taskstate === 1) {

        //             this.shootGiftGroup.visible = true;
        //             PlayerData.GetInstance().shoottaskcurCatchNum = Number(data.step);
        //             PlayerData.GetInstance().shoottasktotalCatchNum = Number(data.stepneed);
        //             PlayerData.GetInstance().shoottaskTime = Number(data.time);
        //             this.needShoot.text = ((PlayerData.GetInstance().shoottasktotalCatchNum - PlayerData.GetInstance().shoottaskcurCatchNum)).toString();
        //             this.giftDes.text = "";
        //             this.giftName.text = "精美礼品";
        //             this.giftPrice.text = "";

        //             let tempxPos: number = this.shootGiftGroup.x;

        //             egret.Tween.get(this.shootGiftGroup).to({ "x": 10 }, 400).to({ "x": 10 }, 800).to({ "x": -200 }, 400);

        //         } else if (data.taskstate === 2) {
        //             this.shootGiftGroup.visible = true;
        //             this.bg.visible = false;
        //             this.bg2.visible = true;
        //             this.itemicon2.visible = false;
        //             this.itemicon.visible = true;
        //             let params = { uid: PlayerData.GetInstance().GetID() };
        //             HttpManager.SendPost(HttpManager.GET_SHOOT_TASKGIFT_TASK_AWARD, this, params, (data) => {
        //                 if (data.code == 200) {
        //                     this.priceImage.visible = true;
        //                     PlayerData.GetInstance().shoottaskTime = Number(data.time);
        //                     this.needShoot.text = "";
        //                     this.giftDes.text = "";
        //                     this.giftName.text = data.giftinfo["name"];
        //                     this.giftPrice.text = "价值约¥" + data.giftinfo["price_src"];
        //                     let temp = data.giftinfo["pic"];
        //                     Helper.SetImageCrossOrigin(this.itemicon, temp);
        //                     HttpManager.RefreshTVMToken(() => {
        //                         this.DisplayDiamondNum();
        //                     });
        //                     let tempxPos: number = this.shootGiftGroup.x;
        //                     egret.Tween.get(this.shootGiftGroup).to({ "x": 10 }, 400).to({ "x": 10 }, 800).to({ "x": -200 }, 400);



        //                 } else {

        //                 }
        //             });
        //         }

        //     }
        // });
    }

    //显示恢复一滴血倒计时
    private ShowRecoveryHP(countdown?: number) {
        if (countdown > 0) {
            this.mRecoveryHP_Time.visible = true;
            this.mRecoveryHP_Text.visible = true;
            this.mRecoveryHP_Time.text = countdown.toString();
        } else {
            this.mRecoveryHP_Time.visible = false;
            this.mRecoveryHP_Text.visible = false;
        }
    }
}
//血量item
class MonsterHpIRUI extends eui.ItemRenderer {
    private mEmptyHp: eui.Image;

    constructor() {
        super();
        this.skinName = "MonsterHpIRSKin";
    }

    protected dataChanged() {
        this.mEmptyHp.visible = this.data.empty;
    }
}