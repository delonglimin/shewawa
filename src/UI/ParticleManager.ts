class ParticleManager {


	public static CreateParticle(parent:egret.DisplayObjectContainer,particleX:number,particleY:number,particlePng:string,particleJson:string):particle.GravityParticleSystem
	{
			let curParticle = new particle.GravityParticleSystem(RES.getRes(particlePng), RES.getRes(particleJson));
			curParticle.x = particleX;
			curParticle.y = particleY;
			curParticle.emitterX =0;
			curParticle.emitterY =0;
			curParticle.touchEnabled =false;
			parent.addChild(curParticle);
			return curParticle;
	}



}