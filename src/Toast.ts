/**
 * Created by egret on 2016/1/26.
 */
class Toast extends egret.DisplayObjectContainer {

    public static Init(cont: egret.DisplayObjectContainer, txtrToastBg: egret.Texture): void {
        //egret.log( "Toast.init", txtrToastBg );
        this._cont = cont;
        this._txtrToastBg = txtrToastBg;
    }

    public static launch(msg: string): void {
        if (this._cont) {
            var toast: Toast = new Toast(msg, this._cont.stage.stageWidth, this._cont.stage.stageHeight);
            this._cont.addChild(toast);
        }
    }

    public static Underlaunch(msg: string,addHeight:number): void {
        if (this._cont) {
            var toast: Toast = new Toast(msg, this._cont.stage.stageWidth, this._cont.stage.stageHeight +addHeight);
            this._cont.addChild(toast);
        }
    }

    private static _txtrToastBg: egret.Texture;
    private static _cont: egret.DisplayObjectContainer;

    constructor(msg: string, w: number, h: number) {
        super();

        let texHeight = Toast._txtrToastBg._bitmapHeight;
        let texWidth = Toast._txtrToastBg._bitmapWidth;

        var bg: egret.Bitmap = new egret.Bitmap(Toast._txtrToastBg);
        let space = Math.min(6, texWidth * .5, texHeight * .5);
        bg.scale9Grid = new egret.Rectangle((texWidth - space) * .5, (texHeight - space) * .5, space, space);
        bg.width = Math.max(480, texWidth);
        this.addChild(bg);

        var tx: egret.TextField = new egret.TextField;
        tx.multiline = true;
        tx.size = 20;
        tx.bold = true;
        tx.textColor = 0xFFFFFF;
        tx.stroke = 0;
        tx.strokeColor = 0;
        tx.text = msg;
        tx.fontFamily = "微软雅黑";
        tx.textAlign = egret.HorizontalAlign.CENTER;
        tx.width = bg.width - 12;
        tx.x = (bg.width - tx.width) * .5;
        bg.height = Math.max(12 + tx.height, texHeight);
        tx.y = (bg.height - tx.height) * .5;;
        this.addChild(tx);

        this.anchorOffsetX = this.width * .5;
        this.anchorOffsetY = this.height * .5;
        this.x = w * .5;
        this.y = h * .618;

        this.alpha = 0;

        egret.Tween.get(this)
            .to({ alpha: 1 }, 800, egret.Ease.quintOut)
            //.to( { scaleX: 1.2, scaleY: 1.2 }, 100, egret.Ease.quintOut )
            //.call( ()=>{ console.log( "tween tween tween" ); } ) 
            //.to( { scaleX: 1.0, scaleY: 1.0 }, 300, egret.Ease.quintIn )
            .wait(1600)
            .to({ alpha: 0 }, 1200, egret.Ease.quintIn).call(() => {
                if (this.parent) {
                    this.parent.removeChild(this);
                }
            });
    }
}