class LifeCycle {
	public static Init() {
        egret.lifecycle.addLifecycleListener((context) => {
            // custom lifecycle plugin
        })

		egret.lifecycle.onPause = () => {

			let shootDollUI = UIManager.GetUIObj(UIName.ShootDollUI) as ShootDollUI;
			if (shootDollUI && shootDollUI.mbIsOpen) {
				shootDollUI.StopBgMusic();
			}
		}

		egret.lifecycle.onResume = () => {
			

			let shootDollUI = UIManager.GetUIObj(UIName.ShootDollUI) as ShootDollUI;
			if (shootDollUI && shootDollUI.mbIsOpen) {
				shootDollUI.PlayBgMusic();
			}
		}
	}
}