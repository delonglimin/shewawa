//////////////////////////////////////////////////////////////////////////////////////
//
//  Copyright (c) 2014-present, Egret Technology.
//  All rights reserved.
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the Egret nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY EGRET AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
//  OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL EGRET AND CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;LOSS OF USE, DATA,
//  OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//////////////////////////////////////////////////////////////////////////////////////

class LoadingUI extends egret.Sprite {
    private static instance:LoadingUI = null;
	public static GetInstance():LoadingUI{
		if(!this.instance){this.instance = new LoadingUI();}
		return this.instance;
	}

    private mProgressBar:eui.ProgressBar;
    public constructor() {
        super();
        // this.createView();

        this.once(eui.UIEvent.ADDED_TO_STAGE, this.createView, this);
    }

    public Display(parent:egret.DisplayObjectContainer){
        this.once(eui.UIEvent.ADDED_TO_STAGE, this.createView, this);
        if(parent) parent.addChild(this);
    }
 

    private createView():void {
        let bg = new eui.Image(RES.getRes("loading_jpg"));
        bg.width = this.stage.stageWidth;
        bg.height = this.stage.stageHeight;
        // bg.percentWidth = 100;
        // bg.percentHeight = 100;
        this.addChild(bg);

        var exml =
            `<e:ProgressBar xmlns:e="http://ns.egret.com/eui">
                <e:Skin>
                    <e:Image width="100%" height="100%" source="barbase_png"/>
                    <e:Image id="thumb" width="100%" height="21" y="-1" source="progressbar_png"/>
                   	<e:Label id="labelDisplay" textAlign="center" verticalAlign="middle"
			                 size="24" fontFamily="Microsoft YaHei" textColor="0xd74d79"
			                 horizontalCenter="0" verticalCenter="-24" text=""/>
                </e:Skin>
            </e:ProgressBar>`;

        var clazz = EXML.parse(exml);
        this.mProgressBar = new clazz();
        this.mProgressBar.width = 582;
        this.mProgressBar.height = 22;
        this.mProgressBar.minimum = 0;
        this.mProgressBar.maximum = 100;
        this.mProgressBar.x = (bg.width - this.mProgressBar.width) / 2;
        this.mProgressBar.y = bg.height - 100;
        this.mProgressBar.labelFunction = (value, max) => {
            let percent:number = value / max * 100;
            return `${Math.floor(percent)}%`;
        };
        this.addChild(this.mProgressBar);
    }

    public setProgress(current:number, total:number):void {
        let percent:number = current/total;
        this.mProgressBar.value = percent * 100;
    }

    public Remove(){
        if(this.parent){
            this.parent.removeChild(this);
        }
    }
}
