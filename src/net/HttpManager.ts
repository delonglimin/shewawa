class HttpManager {
	public static URL: string = "http://dollv2.deerlive.com/"; //weChat正式地址
	public static cdnURL: string = "http://dollv2.deerlive.com/"; 

	private static DEFAULT_URL: string = "Api/SiSi/";
	public static GET_USERINFO: string = HttpManager.DEFAULT_URL+"getUserInfo";GET_SHOOT_DOLL
	public static GET_SHOOT_DOLL: string = "Api/Shoot/getShootList";

	public static ENTER_SHOOL_DOLL: string = "/Api/Shoot/enterShootDoll";
	public static BEIGIN_GAME: string = "/Api/Shoot/beginGame";
	public static REPORT_GAME_RESULT: string = "/Api/Shoot/reportGameResult";

	public static msToken: string = "";
	public static msTvmId: string = "";

	public static SendPost(methodURL: string, thisObj: Object, params?: {}, complete?: Function, onGetProgress?: Function, onIOError?: Function, needToken: boolean = true) {
		let request = new egret.HttpRequest();
		request.responseType = egret.HttpResponseType.TEXT;
		let queryUrl = HttpManager.URL + methodURL;
		//request.open(HttpManager.URL + methodURL, egret.HttpMethod.POST);
		request.open(queryUrl, egret.HttpMethod.POST);
		request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
		if (params) {
			let strParams: string = needToken ? `token=${this.msToken}&` : "";
			for (let key in params) {
				strParams += `${key}=${params[key]}&`;
			}
			strParams = strParams.substring(0, strParams.length - 1);
			request.send(strParams);
		} else {
			if (needToken) request.send(`token=${this.msToken}`);
			else request.send();
		}

		if (complete) {
			request.addEventListener(egret.Event.COMPLETE, (evt) => {
				let request = evt.currentTarget as egret.HttpRequest;
				if (request.response) {
					let data = null;
					try {
						data = JSON.parse(request.response);
					} catch (e) {
						Toast.launch(methodURL + " JSON解析错误: " + request.response);
					}

					if (data) complete(data);
				}

			}, thisObj);
		}

		if (onGetProgress) {
			request.addEventListener(egret.ProgressEvent.PROGRESS, onGetProgress, thisObj);
		}

		request.addEventListener(egret.IOErrorEvent.IO_ERROR, (evt) => {
			console.log("get IO_ERROR: " + methodURL);
			if (onIOError) onIOError(evt);
		}, thisObj);
	}
}