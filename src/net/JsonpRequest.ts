class JsonpRequest {
	
	private static msRequestID:number = 0;
	public static msCompleteCall = {};

	public static Process(url:string, callback:Function, callObj:Object){
		this.msCompleteCall[`call_${this.msRequestID}`] = callback.bind(callObj);
		// console.log(this.msCompleteCall);
		// console.log(JsonpRequest.msCompleteCall);
		
		this.StartLoder(url, this.msRequestID++);
	}

	public static StartLoder(url:string, id:number){
		let script = document.createElement('script');
		script.src = url + `JsonpRequest.msCompleteCall.call_${id}` + "";
		document.body.appendChild(script);
	}

	private static msURL:string = "http://210.73.214.214/Api/SiSi/";
	private static msServerJsonpCallBack = "callback";

	public static Start(method:string, callback:Function, callObj:Object, params?:string){
		let url:string = this.msURL + method + "?";
		if(params) url += params + "&";
		url += `${this.msServerJsonpCallBack}=`; 

		this.Process(url, callback, callObj);
	}

}