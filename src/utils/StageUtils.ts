class StageUtils{
	private static instance: StageUtils = null;
	public static GetInstance(): StageUtils {
		if (!this.instance) { this.instance = new StageUtils(); }
		return this.instance;
	}

	public stage:egret.Stage;

	public Init(stage:egret.Stage){
		this.stage = stage;
	}

	public get stageWidth(){
		return this.stage.stageWidth;
	}

	public get stageHeight(){
		return this.stage.stageHeight;
	}
}