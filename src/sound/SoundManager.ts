class SoundManager {
    private static msSoundMap = {};

    public static Clear(){
        this.msSoundMap = {};
    }
    
    /**创建背景音乐*/
    public static CreateSoundMusic(soundName:string, callBack?:Function){
        this.CreateSound(soundName, egret.Sound.MUSIC, callBack);
    }

    /**创建音效 */
    public static CreateSoundEffect(soundName:string, callBack?:Function){
        this.CreateSound(soundName, egret.Sound.EFFECT, callBack);
    }

    private static CreateSound(soundName, type:string, callBack?:Function){
        let sound = new egret.Sound;
        sound.type = type;
        sound.addEventListener(egret.Event.COMPLETE, ()=>{
            if(callBack) callBack(sound);
            // this.mBgMusicChannel.volume = GameData.GetInstance().GetBgMusicOpen() ? .8 : 0;
            // this.mBgMusicChannel.addEventListener(egret.Event.SOUND_COMPLETE, ()=>{}, this);
        }, this);
        sound.load("resource/sound/" + soundName);
    }

    public static PlaySoundEffect(soundName:string, startTime:number = 0, loops:number = 0){
        this.PlaySound(soundName, egret.Sound.EFFECT, startTime, loops);
    }

    public static StopSoundEffect(soundName:string){
        this.StopSound(soundName, egret.Sound.EFFECT);
    }

    public static PlaySoundMusic(soundName:string, startTime:number = 0, loops:number = 0){
        this.PlaySound(soundName, egret.Sound.MUSIC, startTime, loops);
    }

    public static StopSoundMusic(soundName:string){
        this.StopSound(soundName, egret.Sound.MUSIC);
    }

    private static PlaySound(soundName:string, type:string, startTime:number = 0, loops:number = 0){
        let typeMap = this.msSoundMap[type];
        if(!typeMap){
            this.msSoundMap[type] = {};
            typeMap = this.msSoundMap[type];
        }

        let soundMap = typeMap[soundName];
        if(!soundMap){
            typeMap[soundName] = {};
            soundMap = typeMap[soundName];
        }

        let tempSound = soundMap.sound;
        let sound:egret.Sound = null;
        if(tempSound) sound = tempSound as egret.Sound;

        if(!sound){
            this.CreateSound(soundName, type, (s) => {
                sound = s;
                let channel:egret.SoundChannel = sound.play(startTime, loops);
                channel.volume = this.GetVolume(type);
                soundMap.sound = sound;
                soundMap.channel = channel;
            });
        }else{
            let channel:egret.SoundChannel = sound.play(startTime, loops);
            channel.volume = this.GetVolume(type);
            soundMap.sound = sound;
            soundMap.channel = channel;
        }
    }

    private static StopSound(soundName:string, type:string){
        let typeMap = this.msSoundMap[type];
        if(!typeMap) return;

        let soundMap = typeMap[soundName];
        if(!soundMap) return;

        let tempChannel = soundMap.channel;
        let channel:egret.SoundChannel = null;
        if(tempChannel) channel = tempChannel as egret.SoundChannel;
        if(channel) channel.stop();
    }

    private static GetVolume(type:string):number{
        if(type == egret.Sound.MUSIC){
            return GameData.GetInstance().GetBgMusicOpen() ? 1 : 0;
        }

        if(type == egret.Sound.EFFECT){
            return GameData.GetInstance().GetSoundOpen() ? 1 : 0;
        }

        return 1;
    }
}