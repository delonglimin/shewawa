class GameData {
	private static instance: GameData = null;
	public static GetInstance(): GameData {
		if (!this.instance) { this.instance = new GameData(); }
		return this.instance;
	}

	public mVersion: string = "1.0.0";
	public mOpenID: string = "";
	public mbMicroClient = false;
	public mbRealse = RELEASE;  //egret.Capabilities.isMobile;
	public mQuDao = "www-one";

	private top10RankList: Array<Object> = [];
	public giftList: Array<Object> = [];
	public shootgiftList: Array<Object> = [];

	public exchangeList: Array<Object> = [];


	public IsTVM():boolean{
		return egret.localStorage.getItem("channel")=="tvm";
	}

	public IsWeChat():boolean{
		return egret.localStorage.getItem("channel")=="weChat";
	}

	public GetLocalStorageGameType():string{
		return egret.localStorage.getItem("gameType");
	}

	public RemoveLocalStorageGameType(){
		egret.localStorage.removeItem("gameType");
	}

	public GetBgMusicOpen(): boolean {
		let str = egret.localStorage.getItem('music');
		return str != 'close';
	};
	public SetBgMusicOpen(bgMusicOpen: boolean) {
		egret.localStorage.setItem("music", bgMusicOpen ? 'open' : 'close');
	}

	public GetSoundOpen(): boolean {
		let str = egret.localStorage.getItem('sound');
		return str != 'close';
	};
	public SetSoungOpen(soundOpen: boolean) {
		egret.localStorage.setItem("sound", soundOpen ? 'open' : 'close');
	}

	public GetBarrageOpen(): boolean {
		let str = egret.localStorage.getItem('barrage');
		return str != 'close';
	};

	public SetBarrageOpen(barrageOpen: boolean) {
		egret.localStorage.setItem("barrage", barrageOpen ? 'open' : 'close');

		// if (barrageOpen) {
		// 	RoomBarrageManager.GetInstance().OpenBarrage();
		// 	WorldBarrageManager.GetInstance().OpenBarrage();
		// } else {
		// 	RoomBarrageManager.GetInstance().CloseBarrage();
		// 	WorldBarrageManager.GetInstance().CloseBarrage();
		// }
	}

	public GetShootGuide():boolean{
		let str = egret.localStorage.getItem("shootGuide");
		return !Helper.StrEmptyOrNull(str) && str == "true";
	}

	public SetShootGuide(){
		egret.localStorage.setItem("shootGuide", "true");
	}

	public BEndGuide(guideStep: string): boolean {
		let str = egret.localStorage.getItem(guideStep);
		return str != guideStep;
	};
	public SetGuideStep(guideStep: string) {
		egret.localStorage.setItem(guideStep, guideStep);
	}

	public SetAccount(account: string) {
		egret.localStorage.setItem("account", account);
	}
	public GetAccount(): string {
		return egret.localStorage.getItem("account");
	}
	public SetPassWord(password: string) {
		egret.localStorage.setItem("password", password);
	}
	public GetPassWord(): string {
		return egret.localStorage.getItem("password");
	}

	///设置排行榜前10玩家数据，暂时只在每次进入主界面请求一次。
	public SetTop10(list: Array<Object>) {
		this.top10RankList = list;
	}

	///获取排行榜前10玩家数据
	public GetTop10(): Array<Object> {
		return this.top10RankList;
	}

	public GetRankByName(name: string): number {
		// if(1) return Helper.Random(-5, 5);
		if (!name) return -1;

		if (this.top10RankList) {
			let length = this.top10RankList.length;
			for (let i = 0; i < length; i++) {
				if (this.top10RankList[i]["name"] === name) {
					return i + 1;
				}
			}
		}

		return -1;
	}

	
}