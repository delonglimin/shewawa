class ShootDollData {
	public mName: string = "";
	public mDes: string = "";
	public mTransportInfo: string = "";
	public mImg: string = "";
	
	public mDollID: number = -1;
	public mDollName: string = "";
	public mDeviecID: number = -1;
	public mPrice: number = 10;
	
	public mShootImg: string = "";
	public mNum: number = 0;
	public mbStar: boolean = false;
	public mHp: number = 0;
	public mType: string = "";

	public constructor(data: Object) {
		if (!data) { return; }
		this.mDollName = data["name"];
		this.mDollID = data["id"];
		this.mDeviecID = UICommon.GetShootDeviceIDByDollID(parseInt(this.mDollID.toString()));
		this.mPrice = data["price"];
		this.mShootImg = data["thumb"];
		this.mNum = data["shoot_num"];
		this.mHp = data["blood"];
		this.mbStar = false;
		this.mType = data["shoot_type"];

		let desc = null;
		try {
			desc = data["desc"];
			this.mName = desc.name;
			this.mDes = desc.detail;
			this.mTransportInfo = desc.transportInfo;
			this.mImg = desc.img;
		} catch (e) { }
	}

	public SetCurHp(hp: number) {
		this.mHp = hp;
	}
}