class PlayerData {
	private static instance: PlayerData = null;
	public static GetInstance(): PlayerData {
		if (!this.instance) { this.instance = new PlayerData(); }
		return this.instance;
	}

	private id: string = "";
	public GetID(): string { return this.id; }
	public SetID(id: string) { this.id = id; }

	private name: string = "";
	public GetName(): string { return this.name; };
	public SetName(name: string) { this.name = name; }

	private diamond: number = 0;
	public GetDiamond(): number { return this.diamond; }
	public SetDiamond(diamond: number) {
		this.diamond = diamond;
		//GameEvent.dispatchEvent(GameEvent.EVT_DIAMOND_CHANGE);
	}

	
	public avater: string = "";
	public GetAvater(): string { return this.avater; };
	public SetAvater(avater: string) { this.avater = avater; }
	


	public LoginSuccess(data: Object) {
		if (!data) { egret.log("LoginSuccess: data is null"); return; }
		this.avater = data["avatar"];
		this.id = data["id"];
		this.diamond = parseInt(data["balance"]);
		this.name = data["user_nicename"];
	}


}
