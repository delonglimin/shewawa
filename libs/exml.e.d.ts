declare module skins{
	class ButtonSkin extends eui.Skin{
	}
}
declare module skins{
	class CheckBoxSkin extends eui.Skin{
	}
}
declare module skins{
	class HScrollBarSkin extends eui.Skin{
	}
}
declare module skins{
	class HSliderSkin extends eui.Skin{
	}
}
declare module skins{
	class ItemRendererSkin extends eui.Skin{
	}
}
declare module skins{
	class PanelSkin extends eui.Skin{
	}
}
declare module skins{
	class ProgressBarSkin extends eui.Skin{
	}
}
declare module skins{
	class RadioButtonSkin extends eui.Skin{
	}
}
declare module skins{
	class ScrollerSkin extends eui.Skin{
	}
}
declare module skins{
	class TextInputSkin extends eui.Skin{
	}
}
declare module skins{
	class ToggleSwitchSkin extends eui.Skin{
	}
}
declare module skins{
	class VScrollBarSkin extends eui.Skin{
	}
}
declare module skins{
	class VSliderSkin extends eui.Skin{
	}
}
declare class CharPhraseIRSkin extends eui.Skin{
}
declare class CoinNotEnough extends eui.Skin{
}
declare class IndexListItem extends eui.Skin{
}
declare class SharePhotoSkin extends eui.Skin{
}
declare class MonsterHpIRSKin extends eui.Skin{
}
declare class ShareListItem extends eui.Skin{
}
declare class ShootDollSkin extends eui.Skin{
}
declare class ShootSuccessSkin extends eui.Skin{
}
