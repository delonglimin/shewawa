var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var GameData = (function () {
    function GameData() {
        this.mVersion = "1.0.0";
        this.mOpenID = "";
        this.mbMicroClient = false;
        this.mbRealse = false; //egret.Capabilities.isMobile;
        this.mQuDao = "www-one";
        this.top10RankList = [];
        this.giftList = [];
        this.shootgiftList = [];
        this.exchangeList = [];
    }
    GameData.GetInstance = function () {
        if (!this.instance) {
            this.instance = new GameData();
        }
        return this.instance;
    };
    GameData.prototype.IsTVM = function () {
        return egret.localStorage.getItem("channel") == "tvm";
    };
    GameData.prototype.IsWeChat = function () {
        return egret.localStorage.getItem("channel") == "weChat";
    };
    GameData.prototype.GetLocalStorageGameType = function () {
        return egret.localStorage.getItem("gameType");
    };
    GameData.prototype.RemoveLocalStorageGameType = function () {
        egret.localStorage.removeItem("gameType");
    };
    GameData.prototype.GetBgMusicOpen = function () {
        var str = egret.localStorage.getItem('music');
        return str != 'close';
    };
    ;
    GameData.prototype.SetBgMusicOpen = function (bgMusicOpen) {
        egret.localStorage.setItem("music", bgMusicOpen ? 'open' : 'close');
    };
    GameData.prototype.GetSoundOpen = function () {
        var str = egret.localStorage.getItem('sound');
        return str != 'close';
    };
    ;
    GameData.prototype.SetSoungOpen = function (soundOpen) {
        egret.localStorage.setItem("sound", soundOpen ? 'open' : 'close');
    };
    GameData.prototype.GetBarrageOpen = function () {
        var str = egret.localStorage.getItem('barrage');
        return str != 'close';
    };
    ;
    GameData.prototype.SetBarrageOpen = function (barrageOpen) {
        egret.localStorage.setItem("barrage", barrageOpen ? 'open' : 'close');
        // if (barrageOpen) {
        // 	RoomBarrageManager.GetInstance().OpenBarrage();
        // 	WorldBarrageManager.GetInstance().OpenBarrage();
        // } else {
        // 	RoomBarrageManager.GetInstance().CloseBarrage();
        // 	WorldBarrageManager.GetInstance().CloseBarrage();
        // }
    };
    GameData.prototype.GetShootGuide = function () {
        var str = egret.localStorage.getItem("shootGuide");
        return !Helper.StrEmptyOrNull(str) && str == "true";
    };
    GameData.prototype.SetShootGuide = function () {
        egret.localStorage.setItem("shootGuide", "true");
    };
    GameData.prototype.BEndGuide = function (guideStep) {
        var str = egret.localStorage.getItem(guideStep);
        return str != guideStep;
    };
    ;
    GameData.prototype.SetGuideStep = function (guideStep) {
        egret.localStorage.setItem(guideStep, guideStep);
    };
    GameData.prototype.SetAccount = function (account) {
        egret.localStorage.setItem("account", account);
    };
    GameData.prototype.GetAccount = function () {
        return egret.localStorage.getItem("account");
    };
    GameData.prototype.SetPassWord = function (password) {
        egret.localStorage.setItem("password", password);
    };
    GameData.prototype.GetPassWord = function () {
        return egret.localStorage.getItem("password");
    };
    ///设置排行榜前10玩家数据，暂时只在每次进入主界面请求一次。
    GameData.prototype.SetTop10 = function (list) {
        this.top10RankList = list;
    };
    ///获取排行榜前10玩家数据
    GameData.prototype.GetTop10 = function () {
        return this.top10RankList;
    };
    GameData.prototype.GetRankByName = function (name) {
        // if(1) return Helper.Random(-5, 5);
        if (!name)
            return -1;
        if (this.top10RankList) {
            var length_1 = this.top10RankList.length;
            for (var i = 0; i < length_1; i++) {
                if (this.top10RankList[i]["name"] === name) {
                    return i + 1;
                }
            }
        }
        return -1;
    };
    GameData.instance = null;
    return GameData;
}());
__reflect(GameData.prototype, "GameData");
//# sourceMappingURL=GameData.js.map