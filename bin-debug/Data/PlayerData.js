var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var PlayerData = (function () {
    function PlayerData() {
        this.id = "";
        this.name = "";
        this.diamond = 0;
        this.avater = "";
    }
    PlayerData.GetInstance = function () {
        if (!this.instance) {
            this.instance = new PlayerData();
        }
        return this.instance;
    };
    PlayerData.prototype.GetID = function () { return this.id; };
    PlayerData.prototype.SetID = function (id) { this.id = id; };
    PlayerData.prototype.GetName = function () { return this.name; };
    ;
    PlayerData.prototype.SetName = function (name) { this.name = name; };
    PlayerData.prototype.GetDiamond = function () { return this.diamond; };
    PlayerData.prototype.SetDiamond = function (diamond) {
        this.diamond = diamond;
        //GameEvent.dispatchEvent(GameEvent.EVT_DIAMOND_CHANGE);
    };
    PlayerData.prototype.GetAvater = function () { return this.avater; };
    ;
    PlayerData.prototype.SetAvater = function (avater) { this.avater = avater; };
    PlayerData.prototype.LoginSuccess = function (data) {
        if (!data) {
            egret.log("LoginSuccess: data is null");
            return;
        }
        this.avater = data["avatar"];
        this.id = data["id"];
        this.diamond = parseInt(data["balance"]);
        this.name = data["user_nicename"];
    };
    PlayerData.instance = null;
    return PlayerData;
}());
__reflect(PlayerData.prototype, "PlayerData");
//# sourceMappingURL=PlayerData.js.map