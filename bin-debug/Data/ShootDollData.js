var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var ShootDollData = (function () {
    function ShootDollData(data) {
        this.mName = "";
        this.mDes = "";
        this.mTransportInfo = "";
        this.mImg = "";
        this.mDollID = -1;
        this.mDollName = "";
        this.mDeviecID = -1;
        this.mPrice = 10;
        this.mShootImg = "";
        this.mNum = 0;
        this.mbStar = false;
        this.mHp = 0;
        this.mType = "";
        if (!data) {
            return;
        }
        this.mDollName = data["name"];
        this.mDollID = data["id"];
        this.mDeviecID = UICommon.GetShootDeviceIDByDollID(parseInt(this.mDollID.toString()));
        this.mPrice = data["price"];
        this.mShootImg = data["thumb"];
        this.mNum = data["shoot_num"];
        this.mHp = data["blood"];
        this.mbStar = false;
        this.mType = data["shoot_type"];
        var desc = null;
        try {
            desc = data["desc"];
            this.mName = desc.name;
            this.mDes = desc.detail;
            this.mTransportInfo = desc.transportInfo;
            this.mImg = desc.img;
        }
        catch (e) { }
    }
    ShootDollData.prototype.SetCurHp = function (hp) {
        this.mHp = hp;
    };
    return ShootDollData;
}());
__reflect(ShootDollData.prototype, "ShootDollData");
//# sourceMappingURL=ShootDollData.js.map