var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * Created by egret on 2016/1/26.
 */
var Toast = (function (_super) {
    __extends(Toast, _super);
    function Toast(msg, w, h) {
        var _this = _super.call(this) || this;
        var texHeight = Toast._txtrToastBg._bitmapHeight;
        var texWidth = Toast._txtrToastBg._bitmapWidth;
        var bg = new egret.Bitmap(Toast._txtrToastBg);
        var space = Math.min(6, texWidth * .5, texHeight * .5);
        bg.scale9Grid = new egret.Rectangle((texWidth - space) * .5, (texHeight - space) * .5, space, space);
        bg.width = Math.max(480, texWidth);
        _this.addChild(bg);
        var tx = new egret.TextField;
        tx.multiline = true;
        tx.size = 20;
        tx.bold = true;
        tx.textColor = 0xFFFFFF;
        tx.stroke = 0;
        tx.strokeColor = 0;
        tx.text = msg;
        tx.fontFamily = "微软雅黑";
        tx.textAlign = egret.HorizontalAlign.CENTER;
        tx.width = bg.width - 12;
        tx.x = (bg.width - tx.width) * .5;
        bg.height = Math.max(12 + tx.height, texHeight);
        tx.y = (bg.height - tx.height) * .5;
        ;
        _this.addChild(tx);
        _this.anchorOffsetX = _this.width * .5;
        _this.anchorOffsetY = _this.height * .5;
        _this.x = w * .5;
        _this.y = h * .618;
        _this.alpha = 0;
        egret.Tween.get(_this)
            .to({ alpha: 1 }, 800, egret.Ease.quintOut)
            .wait(1600)
            .to({ alpha: 0 }, 1200, egret.Ease.quintIn).call(function () {
            if (_this.parent) {
                _this.parent.removeChild(_this);
            }
        });
        return _this;
    }
    Toast.Init = function (cont, txtrToastBg) {
        //egret.log( "Toast.init", txtrToastBg );
        this._cont = cont;
        this._txtrToastBg = txtrToastBg;
    };
    Toast.launch = function (msg) {
        if (this._cont) {
            var toast = new Toast(msg, this._cont.stage.stageWidth, this._cont.stage.stageHeight);
            this._cont.addChild(toast);
        }
    };
    Toast.Underlaunch = function (msg, addHeight) {
        if (this._cont) {
            var toast = new Toast(msg, this._cont.stage.stageWidth, this._cont.stage.stageHeight + addHeight);
            this._cont.addChild(toast);
        }
    };
    return Toast;
}(egret.DisplayObjectContainer));
__reflect(Toast.prototype, "Toast");
//# sourceMappingURL=Toast.js.map