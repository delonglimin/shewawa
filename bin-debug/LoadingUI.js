//////////////////////////////////////////////////////////////////////////////////////
//
//  Copyright (c) 2014-present, Egret Technology.
//  All rights reserved.
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the Egret nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY EGRET AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
//  OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL EGRET AND CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;LOSS OF USE, DATA,
//  OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//////////////////////////////////////////////////////////////////////////////////////
var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var LoadingUI = (function (_super) {
    __extends(LoadingUI, _super);
    function LoadingUI() {
        var _this = _super.call(this) || this;
        // this.createView();
        _this.once(eui.UIEvent.ADDED_TO_STAGE, _this.createView, _this);
        return _this;
    }
    LoadingUI.GetInstance = function () {
        if (!this.instance) {
            this.instance = new LoadingUI();
        }
        return this.instance;
    };
    LoadingUI.prototype.Display = function (parent) {
        this.once(eui.UIEvent.ADDED_TO_STAGE, this.createView, this);
        if (parent)
            parent.addChild(this);
    };
    LoadingUI.prototype.createView = function () {
        var bg = new eui.Image(RES.getRes("loading_jpg"));
        bg.width = this.stage.stageWidth;
        bg.height = this.stage.stageHeight;
        // bg.percentWidth = 100;
        // bg.percentHeight = 100;
        this.addChild(bg);
        var exml = "<e:ProgressBar xmlns:e=\"http://ns.egret.com/eui\">\n                <e:Skin>\n                    <e:Image width=\"100%\" height=\"100%\" source=\"barbase_png\"/>\n                    <e:Image id=\"thumb\" width=\"100%\" height=\"21\" y=\"-1\" source=\"progressbar_png\"/>\n                   \t<e:Label id=\"labelDisplay\" textAlign=\"center\" verticalAlign=\"middle\"\n\t\t\t                 size=\"24\" fontFamily=\"Microsoft YaHei\" textColor=\"0xd74d79\"\n\t\t\t                 horizontalCenter=\"0\" verticalCenter=\"-24\" text=\"\"/>\n                </e:Skin>\n            </e:ProgressBar>";
        var clazz = EXML.parse(exml);
        this.mProgressBar = new clazz();
        this.mProgressBar.width = 582;
        this.mProgressBar.height = 22;
        this.mProgressBar.minimum = 0;
        this.mProgressBar.maximum = 100;
        this.mProgressBar.x = (bg.width - this.mProgressBar.width) / 2;
        this.mProgressBar.y = bg.height - 100;
        this.mProgressBar.labelFunction = function (value, max) {
            var percent = value / max * 100;
            return Math.floor(percent) + "%";
        };
        this.addChild(this.mProgressBar);
    };
    LoadingUI.prototype.setProgress = function (current, total) {
        var percent = current / total;
        this.mProgressBar.value = percent * 100;
    };
    LoadingUI.prototype.Remove = function () {
        if (this.parent) {
            this.parent.removeChild(this);
        }
    };
    LoadingUI.instance = null;
    return LoadingUI;
}(egret.Sprite));
__reflect(LoadingUI.prototype, "LoadingUI");
//# sourceMappingURL=LoadingUI.js.map