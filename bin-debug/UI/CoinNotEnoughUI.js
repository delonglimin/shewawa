var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var CoinNotEnoughUI = (function (_super) {
    __extends(CoinNotEnoughUI, _super);
    function CoinNotEnoughUI() {
        return _super.call(this) || this;
    }
    CoinNotEnoughUI.prototype.GetExmlName = function () { return "CoinNotEnough.exml"; };
    CoinNotEnoughUI.prototype.RegisterBtnEvt = function () {
        this.mBackBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.BackBtn, this);
        this.RechargeBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.GoRecharge, this);
    };
    CoinNotEnoughUI.prototype.RemoveBtnEvt = function () {
        this.mBackBtn.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.BackBtn, this);
        this.RechargeBtn.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.GoRecharge, this);
    };
    CoinNotEnoughUI.prototype.GoRecharge = function () {
        this.BackBtn();
    };
    return CoinNotEnoughUI;
}(BaseUI));
__reflect(CoinNotEnoughUI.prototype, "CoinNotEnoughUI");
//# sourceMappingURL=CoinNotEnoughUI.js.map