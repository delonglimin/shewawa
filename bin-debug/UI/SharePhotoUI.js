var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var SharePhotoUI = (function (_super) {
    __extends(SharePhotoUI, _super);
    function SharePhotoUI() {
        var _this = _super.call(this) || this;
        _this.mDataArray = [];
        return _this;
    }
    SharePhotoUI.prototype.GetExmlName = function () { return "SharePhotoSkin.exml"; };
    SharePhotoUI.prototype.Open = function () {
        _super.prototype.Open.call(this);
        this.SetDataArray();
        this.Refresh();
    };
    SharePhotoUI.prototype.Close = function () {
        _super.prototype.Close.call(this);
        this.mDataArray = [];
    };
    SharePhotoUI.prototype.RegisterBtnEvt = function () {
        this.mCloseBtn.once(egret.TouchEvent.TOUCH_TAP, this.BackBtn, this);
    };
    SharePhotoUI.prototype.SetDataArray = function () {
        this.mDataArray = [];
        var strDes = [];
        strDes.push("一大早就收到快递，一看是射娃娃中的iphoneX！太开心啦，一天都有好心情，忍不住再上去玩几把，看我今天还能中什么，嘻嘻~");
        strDes.push("昨天第一天玩就射中个手机，今天就收到了！是我运气太好还是真那么好打？我要再试试看去咯~");
        strDes.push("这娃娃好可爱，比想象中的还要好，手感一级棒，就想抱着睡！");
        strDes.push("收到中的洁面仪了，可以不用去买啦，听说这个是网红产品效果棒棒哒！");
        strDes.push("真是巧了！手机刚坏就中了个新的，真是太及时了！谢谢射娃娃，哈哈哈，我要再去射中一个！");
        strDes.push("前些天射中的平衡车到了，儿子玩得不亦乐乎，村里的小盆友都排队要来玩。");
        strDes.push("儿子回家了，好开心，晒个图。花了10块钱，好像比买合算。");
        strDes.push("送给小朋友当礼物了，一起寄了几个，给小宝宝，果然萌物更受小朋友的喜爱，自己都忍不住想再来射一只了");
        strDes.push("宝贝很快就收到了，呆呆的，萌萌的，很可爱！");
        strDes.push("最近抓娃娃完全停不下来啊，一会就那么多了，是不是很厉害？");
        var names = [];
        names.push("卡布奇诺式爱恋╮");
        names.push("金枪丶小帅");
        names.push("平靜滴壞♡情");
        names.push("那时明月");
        names.push("蛋蛋的心");
        names.push("秋殇别恋");
        names.push("邂逅黄昏");
        names.push("﹎麦芽糖");
        names.push("習慣了你");
        names.push("抠脚大汉");
        var photoArrary = [];
        photoArrary.push(new Array("photo1_1_jpg", "photo1_2_jpg"));
        photoArrary.push(new Array("photo2_jpg"));
        photoArrary.push(new Array("photo3_jpg"));
        photoArrary.push(new Array("photo4_1_jpg", "photo4_2_jpg"));
        photoArrary.push(new Array("photo5_1_jpg", "photo5_2_jpg"));
        photoArrary.push(new Array("photo6_1_jpg", "photo6_2_jpg"));
        photoArrary.push(new Array("photo7_jpg"));
        photoArrary.push(new Array("photo8_jpg"));
        photoArrary.push(new Array("photo9_jpg"));
        photoArrary.push(new Array("photo10_jpg"));
        for (var i = 0; i < 10; i++) {
            this.mDataArray.push({ des: strDes[i], name: names[i], photo: photoArrary[i], avatar: "avatar_" + (i + 1) + "_jpg", });
        }
    };
    SharePhotoUI.prototype.Refresh = function () {
        this.mList.dataProvider = new eui.ArrayCollection(this.mDataArray);
        this.mList.itemRenderer = ShareListItemUI;
    };
    return SharePhotoUI;
}(BaseUI));
__reflect(SharePhotoUI.prototype, "SharePhotoUI");
var ShareListItemUI = (function (_super) {
    __extends(ShareListItemUI, _super);
    function ShareListItemUI() {
        var _this = _super.call(this) || this;
        _this.skinName = "ShareListItem";
        return _this;
    }
    ShareListItemUI.prototype.dataChanged = function () {
        var avatar = this.data.avatar;
        var name = this.data.name;
        var des = this.data.des;
        var photoArr = this.data.photo;
        this.shareicon.source = avatar;
        this.shareicon.mask = this.shareiconMask;
        this.shareImage.visible = false;
        this.shareImage1.visible = false;
        if (photoArr && photoArr.length > 0) {
            this.shareImage.visible = true;
            this.shareImage.source = photoArr[0];
            if (photoArr.length > 1) {
                this.shareImage1.visible = true;
                this.shareImage1.source = photoArr[1];
            }
        }
        this.shareName.text = name;
        this.shareDes.text = des;
    };
    return ShareListItemUI;
}(eui.ItemRenderer));
__reflect(ShareListItemUI.prototype, "ShareListItemUI");
//# sourceMappingURL=SharePhotoUI.js.map