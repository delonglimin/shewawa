var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var IndexUI = (function (_super) {
    __extends(IndexUI, _super);
    function IndexUI() {
        var _this = _super.call(this) || this;
        _this.mDataArray = [];
        return _this;
    }
    IndexUI.prototype.GetExmlName = function () { return "IndexUI.exml"; };
    IndexUI.prototype.Open = function () {
        _super.prototype.Open.call(this);
        this.SetDataArray();
        this.Refresh();
    };
    IndexUI.prototype.Close = function () {
        _super.prototype.Close.call(this);
        this.mDataArray = [];
    };
    IndexUI.prototype.SetDataArray = function () {
        this.mDataArray = [];
        // let data = {
        //          desc: {
        //             "name":"Apple/苹果 iPhone X",
        //             "detail":"银色，深空灰色",
        //             "transportInfo":"随机发货",
        //             img :"http://103.244.233.243/data/upload/20180216/5a86ce389d2e6.jpg"
        //          },
        //         id : "143",
        //         blood:"3",
        //         name :"Apple/苹果 iPhone X",
        //         shoot_num:11,//射中次数
        //         thumb :"http://103.244.233.243/data/upload/20180211/5a7fa5c48e238.png",
        //         price:"10"
        // };
        // for(let i = 0; i  < 10; i++){
        // 	this.mDataArray.push(data);
        // }
        this.mListArrayCollection = new eui.ArrayCollection(this.mDataArray);
        this.mList.dataProvider = this.mListArrayCollection;
        this.mList.itemRenderer = IndexListItemUI;
    };
    IndexUI.prototype.Refresh = function () {
        this.RequestShootDollsList();
    };
    IndexUI.prototype.RequestShootDollsList = function () {
        var _this = this;
        var params = {};
        HttpManager.SendPost(HttpManager.GET_SHOOT_DOLL, this, params, function (data) {
            var code = data.code;
            if (code === 200) {
                var dollData = data['data'];
                _this.mDataArray = [];
                for (var _i = 0, dollData_1 = dollData; _i < dollData_1.length; _i++) {
                    var item = dollData_1[_i];
                    _this.mDataArray.push(item);
                }
                _this.mListArrayCollection.replaceAll(_this.mDataArray);
            }
            else {
                Toast.launch(data.descrp);
            }
        });
    };
    return IndexUI;
}(BaseUI));
__reflect(IndexUI.prototype, "IndexUI");
var IndexListItemUI = (function (_super) {
    __extends(IndexListItemUI, _super);
    function IndexListItemUI() {
        var _this = _super.call(this) || this;
        _this.skinName = "IndexListItem";
        return _this;
    }
    IndexListItemUI.prototype.createChildren = function () {
        var _this = this;
        _super.prototype.createChildren.call(this);
        this.mIcon.addEventListener(egret.TouchEvent.TOUCH_TAP, function (evt) {
            UIManager.OpenUI(UIName.ShootDollUI, _this.parent.parent.parent, function (openUI) {
                if (openUI) {
                    openUI.SetData("shootDollData", _this.data);
                }
            });
        }, this);
    };
    IndexListItemUI.prototype.dataChanged = function () {
        var icon = this.data.desc.img;
        var name = this.data.name;
        var cost = this.data.price;
        var num = "\u5C04\u4E2D" + this.data.shoot_num + "\u6B21";
        this.mItemName.text = name;
        this.mCostNum.text = cost;
        this.mNum.text = num;
        Helper.SetImageCrossOrigin(this.mIcon, icon);
    };
    return IndexListItemUI;
}(eui.ItemRenderer));
__reflect(IndexListItemUI.prototype, "IndexListItemUI");
//# sourceMappingURL=IndexUI.js.map