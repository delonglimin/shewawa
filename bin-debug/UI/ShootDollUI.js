var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var ShootDollUI = (function (_super) {
    __extends(ShootDollUI, _super);
    //==============================login===========================================
    function ShootDollUI() {
        var _this = _super.call(this) || this;
        _this.mChatMaxCount = 10;
        _this.mBulletTweening = false;
        _this.mbShoot = false;
        _this.mMonsterTotalHp = 3;
        _this.mMonsterCurHp = 3;
        _this.mBulletStartX = 0;
        _this.mBulletStartY = 0;
        _this.mThroughCircleMinAngle = 0;
        _this.mThroughCircleMaxAngle = 0;
        _this.mCircleCurAngle = 0;
        _this.mCircleDefaultAddAngle = 3 / 16;
        _this.mCircleAddAngle = 3 / 16;
        _this.mCircleAddAngleRate = 1;
        // private mBulletCurAngle = 0;
        _this.mBulletDefaultAddPosY = 7.5 / 16;
        _this.mBulletAddPosY = 7.5 / 16;
        _this.mBulletAddAngle = 10 / 16;
        _this.mBulletTargetPosY = 0;
        _this.mBulletMoveDistanceForCalculation = 0;
        // private mBulletCurTickCount = 0;
        _this.mBulletDelayTickCount = 0;
        _this.mBulletDelayMaxTickCount = 20 / 60 * 1000;
        _this.mbLineTweenToLeft = false;
        _this.mOnlineNum = 0;
        _this.mOnlineUserAvatarArray = [];
        _this.mID = "";
        _this.mName = "";
        _this.mAvatar = "";
        _this.mDeviceID = 0;
        _this.mRemoteUserID = 0;
        _this.mChannel = 0;
        _this.game_log_id = "";
        // private mSocket: SocketIOClient.Socket;
        _this.NORMAL = 1;
        _this.mbGuide = true;
        return _this;
    }
    ShootDollUI.prototype.GetExmlName = function () { return "ShootDollSkin.exml"; };
    ShootDollUI.prototype.RegisterBtnEvt = function () {
        this.mBackBtn.once(egret.TouchEvent.TOUCH_TAP, this.BackBtn, this);
        this.mRecordBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.RecordBtn, this);
        this.mChatBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.ChatBtn, this);
        this.mChatVisibleToggle.addEventListener(egret.TouchEvent.TOUCH_TAP, this.ChatVisibleToggle, this);
        this.mChatSendBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.ChatSendBtn, this);
        this.mChatPhraseVisibleToggle.addEventListener(egret.TouchEvent.TOUCH_TAP, this.ChatPhraseVisibleToggle, this);
        this.mCloseChatPhraseBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.CloseChatHandleBtn, this);
        this.mPayBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.PayBtn, this);
        this.mDollDetailBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.DollDetailBtn, this);
        this.mStarToggle.addEventListener(egret.TouchEvent.TOUCH_TAP, this.StarToggle, this);
        this.mShootBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.ShootBtn, this);
        // this.mSameList.addEventListener(eui.ItemTapEvent.ITEM_TAP, this.ClickSameItem, this);
        // this.mSameRightBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.SameBtn, this);
        // this.mSameLeftBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.SameBtn, this);
        this.ShareBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.OpenShare, this);
        this.mCloseGuideBtn.once(egret.TouchEvent.TOUCH_TAP, this.CloseGuide, this);
    };
    ShootDollUI.prototype.RemoveBtnEvt = function () {
        this.mRecordBtn.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.RecordBtn, this);
        this.mChatBtn.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.ChatBtn, this);
        this.mChatVisibleToggle.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.ChatVisibleToggle, this);
        this.mChatSendBtn.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.ChatSendBtn, this);
        this.mChatPhraseVisibleToggle.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.ChatPhraseVisibleToggle, this);
        this.mCloseChatPhraseBtn.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.CloseChatHandleBtn, this);
        this.mPayBtn.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.PayBtn, this);
        this.mDollDetailBtn.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.DollDetailBtn, this);
        this.mStarToggle.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.StarToggle, this);
        this.mShootBtn.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.ShootBtn, this);
        // this.mSameList.removeEventListener(eui.ItemTapEvent.ITEM_TAP, this.ClickSameItem, this);
        // this.mSameRightBtn.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.SameBtn, this);
        // this.mSameLeftBtn.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.SameBtn, this);
        this.ShareBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.OpenShare, this);
    };
    ShootDollUI.prototype.RegisterGameEvt = function () {
        this.addEventListener(egret.Event.ENTER_FRAME, this.EnterFrame, this);
        // egret.startTick(this.StartTick, this);
    };
    ShootDollUI.prototype.RemoveGameEvt = function () {
        this.removeEventListener(egret.Event.ENTER_FRAME, this.EnterFrame, this);
        // egret.stopTick(this.StartTick, this);
    };
    //open--registerBtn registerGame ---init
    ShootDollUI.prototype.Open = function () {
        _super.prototype.Open.call(this);
        this.Init(); //初始化游戏信息 开始绳子动画
        this.InitRotateInfo(); //初始化子弹数据和旋转的环 距离   角度设定等信息
        //this.VisibleGuide();//初始引导
        this.DisplayShootInfo(); //显示要射击的图片
        this.InitMonsterHpInfo(); //显示血量信息
        this.getCurMonsterHp();
        //this.DisplayStarStatus();//显示是否收藏
        this.DisplayDiamondNum(); //显示用户余额等信息  是金币还是娃娃券
        this.DisplayCostNum(); //显示需要花费多少金币 或者券
        // this.DisplaySameInfo();//显示空闲   同类型机器
        this.RequestDollDetailInfo(); //获取并显示奖品详情
        //this.InitBarrage();//初始化房间弹幕
        //this.InitAudienceAvatar();//初始化观众信息  在线人数 和前三位
        // this.RefreshChatDisplayedInfo();//聊天信息显示收藏控制
        this.VisibleChatHandledInfo(false); //聊天助手显示隐藏
        this.VisibleDollDetailInfo(true); //显示娃娃详情
        this.VisibleRecordGroup(false); //房间记录
        this.DisplayUsingInfo(); //显示谁在游戏等信息
        this.PlayBgMusic(); //播放背景音乐
        this.ShowRecoveryHP(); //显示恢复一滴血倒计时
    };
    ShootDollUI.prototype.Close = function () {
        egret.Tween.removeTweens(this.mLineParent);
        this.mLineParent.rotation = 0;
        this.StopBgMusic();
        this.StopBarrage();
        this.mOnlineUserAvatarArray = [];
        //该方法用于退出当前的频道。退出成功后，频道内的所有其他用户将收到 onChannelUserLeaved 回调。
        // this.mChannel.channelLeave(() => { });
        // this.agoraSession.logout();
        this.StopHitEffect();
        this.ResetShootAvatarInfo();
        _super.prototype.Close.call(this);
    };
    //初始化游戏信息 开始绳子动画
    ShootDollUI.prototype.Init = function () {
        this.mbGuide = true;
        this.mTimeEnterFrame = egret.getTimer();
        this.mbShoot = false; //服务器通过  客户端也通过  则认为可中奖
        this.mBulletTweening = false;
        this.mBullet.visible = false;
        this.mBulletDelayTickCount = 0;
        this.mBulletTargetPosY = 0;
        this.mShootAvatarInitPosY = this.mShootAvatar.y;
        this.mScroller.bounces = false;
        this.mScroller.viewport.scrollV = 0;
        this.mID = PlayerData.GetInstance().GetID().toString();
        this.mName = PlayerData.GetInstance().GetName();
        this.mAvatar = PlayerData.GetInstance().GetAvater();
        this.mChatTextFlow = [];
        var dataObj = this.GetData("shootDollData");
        this.mShootDollData = new ShootDollData(dataObj);
        this.mDeviceID = this.mShootDollData.mDeviecID;
        this.mRemoteUserID = this.mDeviceID;
        this.mChannel = this.mDeviceID;
        this.mShootNum.text = "已射中" + this.mShootDollData.mNum + "次";
        //let signal = Signal("b4fdddb559d241e58c9adad9ef266cb4");
        //this.agoraSession = signal.login(PlayerData.GetInstance().GetID(), PlayerData.GetInstance().mSignalingKey);
        // this.agoraSession.onLoginSuccess = (data) => {
        //     this.agoraSession.invoke("io.agora.signal.user_set_attr", { "name": "icon", "value": this.mAvatar }, (err, val) => {
        //         this.DoJoin(this.mDeviceID);
        //     });
        // }
        // this.agoraSession.onLoginFailed = (ecode) => { Toast.launch("登录失败" + ecode); }
        // this.agoraSession.onError = (evt) => { Toast.launch("登录错误" + evt) }
        // this.agoraSession.onMessageInstantReceive = (account, uid, data) => { };
        //绳子动画
        this.NewTweenLine();
    };
    ShootDollUI.prototype.InitBarrage = function () {
        //RoomBarrageManager.GetInstance().Init(this);
    };
    ShootDollUI.prototype.StopBarrage = function () {
        // RoomBarrageManager.GetInstance().CloseBarrage();
    };
    //绳子动画
    ShootDollUI.prototype.NewTweenLine = function () {
        var iTween = egret.Tween.get(this.mLineParent, { "loop": true });
        iTween.to({ "rotation": 5 }, 1500).to({ "rotation": -5 }, 3000).to({ "rotation": 0 }, 1500);
    };
    //圆环  子弹 动画
    ShootDollUI.prototype.EnterFrame = function () {
        var now = egret.getTimer();
        var pass = now - this.mTimeEnterFrame;
        this.mTimeEnterFrame = egret.getTimer();
        this.Update(pass);
    };
    ShootDollUI.prototype.Update = function (deltaTime) {
        // egret.log(deltaTime);
        //this.TweenLine(deltaTime);
        this.UpdateCircleAngle(deltaTime);
        this.UpdateBulletInfo(deltaTime);
    };
    //Discarded
    ShootDollUI.prototype.StartTick = function (time) {
        // egret.log("StartTick" + time);
        //this.TweenLine(time);
        this.UpdateCircleAngle(time);
        this.UpdateBulletInfo(time);
        return true;
    };
    //初始化子弹数据和旋转的环 距离   角度设定等信息
    ShootDollUI.prototype.InitRotateInfo = function () {
        // this.mCircleAddAngleRate = Helper.Random(5, 15) / 5;
        this.mCircleAddAngle = this.mCircleDefaultAddAngle;
        this.mCircleAddAngle *= this.mCircleAddAngleRate;
        this.mBulletAddPosY = this.mBulletDefaultAddPosY;
        this.mBulletStartY = this.height - this.mBulletStartPoint.bottom;
        this.mBulletStartX = this.mBulletStartPoint.x;
        this.mBulletMoveDistanceForCalculation = this.mBulletStartY - this.mRotateCircle.y - this.mRotateCircle.height * .5;
        this.mThroughCircleMinAngle = 210;
        this.mThroughCircleMaxAngle = 270;
    };
    //显示血量信息
    ShootDollUI.prototype.InitMonsterHpInfo = function () {
        //初始化当前剩余血量
        this.mMonsterTotalHp = this.mShootDollData.mHp;
        this.mMonsterCurHp = this.mShootDollData.mHp;
        var data = [];
        for (var i = 0; i < this.mMonsterTotalHp; i++) {
            var bEmpty = this.mMonsterCurHp <= i;
            data.push({ empty: bEmpty });
        }
        this.mHpCollection = new eui.ArrayCollection(data);
        var gap = 0;
        var horizontalLayout = this.mHpList.layout;
        if (horizontalLayout)
            gap = horizontalLayout.gap;
        this.mHpList.width = this.mMonsterTotalHp * 40 + (this.mMonsterTotalHp - 1) * gap;
        this.mHpList.dataProvider = this.mHpCollection;
        this.mHpList.itemRenderer = MonsterHpIRUI;
    };
    //获取当前用户 当前娃娃的剩余血量
    ShootDollUI.prototype.getCurMonsterHp = function () {
        var _this = this;
        var params = { id: this.mShootDollData.mDollID };
        HttpManager.SendPost(HttpManager.ENTER_SHOOL_DOLL, this, params, function (data) {
            var code = data.code;
            if (code === 200) {
                var dollData = data['data'];
                _this.mMonsterCurHp = _this.mMonsterTotalHp - dollData.current;
                _this.DisplayMonsterHp();
            }
            else {
                Toast.launch(data.descrp);
            }
        });
    };
    //显示要射击的图片
    ShootDollUI.prototype.DisplayShootInfo = function () {
        Helper.SetImageCrossOrigin(this.mShootAvatar, this.mShootDollData.mShootImg);
        // Helper.ImageAddMask(this.mShootAvatar);
    };
    ShootDollUI.prototype.DisplayMonsterHp = function () {
        var data = [];
        for (var i = 0; i < this.mMonsterTotalHp; i++) {
            var bEmpty = this.mMonsterCurHp <= i;
            data.push({ empty: bEmpty });
        }
        this.mHpCollection.replaceAll(data);
    };
    ShootDollUI.prototype.UpdateCircleAngle = function (deltaTime) {
        this.mCircleCurAngle += this.mCircleAddAngle * deltaTime;
        //(0, 180) (-180, 0)
        if (this.mCircleCurAngle >= 180) {
            this.mCircleCurAngle -= 360;
        }
        this.mRotateCircle.rotation = this.mCircleCurAngle;
    };
    ShootDollUI.prototype.UpdateBulletInfo = function (deltaTime) {
        var _this = this;
        if (this.mBulletTargetPosY <= 0)
            return;
        if (this.mBulletDelayTickCount > 0) {
            this.mBulletDelayTickCount -= deltaTime;
            return;
        }
        if (!this.mBullet.visible)
            this.mBullet.visible = true;
        var curAngle = this.mBulletStar.rotation;
        curAngle += this.mBulletAddAngle * deltaTime;
        if (curAngle >= 180) {
            curAngle -= 360;
        }
        this.mBulletStar.rotation = curAngle;
        if (this.mBulletTweening)
            return;
        this.mBullet.y -= this.mBulletAddPosY * deltaTime;
        if (this.mBullet.y <= this.mBulletTargetPosY) {
            this.mBullet.y = this.mBulletTargetPosY;
            if (this.mbShoot) {
                this.mBulletTargetPosY = 0;
                this.mBullet.visible = false;
                this.StopBulletDragEffect();
                this.PlayHitEffect();
                this.PlayHitSoundEffect();
                this.PlayWinSoundEffect();
                this.mMonsterCurHp--;
                this.DisplayMonsterHp();
                if (this.mMonsterCurHp <= 0) {
                    this.TweenShootAvatar(function () {
                        _this.ShootDollSuccess();
                        _this.RefreshShootDollInfo();
                        _this.ResetShootAvatarInfo();
                        _this.reportGameResult("success");
                    });
                }
                else {
                    this.reportGameResult("success");
                    var msg = this.GetShootBarrage(this.mName); //获取射中的描述字符串
                    this.DisplayRoomBarrage(this.mAvatar, msg, this.mName);
                    this.PlayeEffect();
                }
                this.mbShoot = false;
            }
            else {
                var msg = this.GetNotShootBarrage(this.mName); //获取没有射中的描述字符串
                //this.DisplayRoomBarrage(this.mAvatar, msg, this.mName);
                this.PlayHitMissSoundEffect();
                egret.Tween.removeTweens(this.mBullet);
                var bRight = Helper.Random(0, 2) == 1; //随机  让子弹星星  偏左 还是偏右
                var addX = Helper.Random(100, 200);
                var targetX = this.mBullet.x + (bRight ? addX : -addX);
                var targetY = this.mBullet.y + Helper.Random(100, 200);
                this.mBulletTweening = true;
                egret.Tween.get(this.mBullet).to({ x: targetX, y: targetY }, 400, egret.Ease.sineIn).call(function () {
                    _this.mBulletTargetPosY = 0;
                    _this.mBulletTweening = false;
                    _this.mBullet.visible = false;
                    _this.StopBulletDragEffect();
                });
                this.reportGameResult("fail");
            }
            //this.GiftTween();//显示再射几次 获取精美礼品
            this.mBulletAddPosY = this.mBulletDefaultAddPosY;
            this.mCircleAddAngle = this.mCircleDefaultAddAngle;
            this.mCircleAddAngle *= this.mCircleAddAngleRate;
        }
    };
    ShootDollUI.prototype.PlayHitEffect = function () {
        var _this = this;
        if (!this.mHitEffectPoint)
            this.mHitEffectPoint = ParticleManager.CreateParticle(this.mRotateCircle.parent, this.mRotateCircle.x, this.mRotateCircle.y, "hit_effect_dian_png", "hit_effect_dian_json");
        if (!this.mHitEffectStar)
            this.mHitEffectStar = ParticleManager.CreateParticle(this.mRotateCircle.parent, this.mRotateCircle.x, this.mRotateCircle.y, "hit_effect_star_png", "hit_effect_star_json");
        this.mHitEffectPoint.start();
        this.mHitEffectStar.start();
        this.mHitSetTimeOut = setTimeout(function () { return _this.StopHitEffect(); }, 500);
    };
    ShootDollUI.prototype.StopHitEffect = function () {
        if (this.mHitSetTimeOut)
            clearTimeout(this.mHitSetTimeOut);
        if (this.mHitEffectPoint)
            this.mHitEffectPoint.stop();
        if (this.mHitEffectStar)
            this.mHitEffectStar.stop();
    };
    //，服务器不让中，则修改角度
    ShootDollUI.prototype.RotateChange = function () {
        var curRotation = this.mRotateCircle.rotation;
        if (curRotation < 0)
            curRotation += 360;
        var bulletCurTickCount = this.mBulletMoveDistanceForCalculation / this.mBulletAddPosY;
        var virtualRealRotation = curRotation + this.mCircleAddAngle * bulletCurTickCount;
        var realRotation = virtualRealRotation % 360;
        var bThroughCircle = realRotation >= this.mThroughCircleMinAngle && realRotation <= this.mThroughCircleMaxAngle;
        //如果本身能中  ，服务器不让中，则修改角度
        if (this.mbShoot != bThroughCircle) {
            var targetRotation = realRotation;
            var space = Math.abs(this.mThroughCircleMaxAngle - this.mThroughCircleMinAngle);
            if (this.mbShoot) {
                targetRotation = Helper.Random(this.mThroughCircleMinAngle + space * .25, this.mThroughCircleMaxAngle - space * .25);
            }
            else {
                if (Math.abs(this.mThroughCircleMaxAngle - realRotation) < Math.abs(this.mThroughCircleMinAngle - realRotation)) {
                    targetRotation = Helper.Random(this.mThroughCircleMaxAngle, this.mThroughCircleMaxAngle + space * .25);
                }
                else {
                    targetRotation = Helper.Random(this.mThroughCircleMinAngle, this.mThroughCircleMinAngle - space * .25);
                }
            }
            var addRotation = targetRotation - realRotation;
            if (addRotation > 180) {
                addRotation -= 360;
            }
            this.mBulletDelayTickCount = 0;
            var bulletExtraAddPos = 0;
            var circleExtraAddAngle = 0;
            var bNeedAdjustBulletExtraAddPos = true;
            if (addRotation > 0) {
                this.mBulletDelayTickCount = Math.min(this.mBulletDelayMaxTickCount, addRotation / this.mCircleAddAngle);
                var extraAddRotation = addRotation - this.mBulletDelayTickCount * this.mCircleAddAngle;
                ;
                if (extraAddRotation <= 0) {
                    circleExtraAddAngle = extraAddRotation / (bulletCurTickCount + this.mBulletDelayTickCount);
                    bNeedAdjustBulletExtraAddPos = false;
                }
            }
            if (bNeedAdjustBulletExtraAddPos) {
                circleExtraAddAngle = addRotation / (bulletCurTickCount + this.mBulletDelayTickCount);
                var index = 1;
                while (true) {
                    if (index > 6)
                        break;
                    if (index & 1) {
                        bulletExtraAddPos = this.GetBulletExtraAddAngle(circleExtraAddAngle * .5, addRotation);
                    }
                    else {
                        circleExtraAddAngle = this.GetCircleExtraAddAngle(bulletExtraAddPos * .5, addRotation);
                    }
                    index++;
                }
            }
            this.mCircleAddAngle += circleExtraAddAngle;
            this.mBulletAddPosY += bulletExtraAddPos;
            // console.log(`v=${this.mBulletAddPosY}; x=${this.mCircleAddAngle}; s=${this.mBulletMoveDistanceForCalculation}; B=${curRotation}; y0=${realRotation}; t0=${this.mBulletdelayTickCount / 60}; y1=${targetRotation}; angle=${addRotation}`);
        }
        this.mRotationDes.text = this.mbShoot.toString();
    };
    //y:需要调整的角度； s：子弹移动的距离； x：环的转速；x0：环的默认转速
    //v:子弹速度；v0：子弹默认速度；t0：子弹发射延迟时间
    //y = x0 * t0 + (x / v - x0 / v0) * s;
    /**
     * 获取额外的环的加速
     * x = v * ((y - x0 * t0) / s + x0 / v0)
    */
    ShootDollUI.prototype.GetCircleExtraAddAngle = function (bulletExtraAddPos, addRotation) {
        var v = this.mBulletAddPosY + bulletExtraAddPos;
        var y = addRotation;
        var x0 = this.mCircleAddAngle;
        var t0 = this.mBulletDelayTickCount;
        var s = this.mBulletMoveDistanceForCalculation;
        var v0 = this.mBulletAddPosY;
        var x = v * ((y - x0 * t0) / s + x0 / v0);
        return x - x0;
    };
    /**
     * 获取额外的子弹的加速
     * v = x / ((y - x0 * t0) / s + x0 / v0)
    */
    ShootDollUI.prototype.GetBulletExtraAddAngle = function (circleExtraAddAngle, addRotation) {
        var x = this.mCircleAddAngle + circleExtraAddAngle;
        var y = addRotation;
        var x0 = this.mCircleAddAngle;
        var t0 = this.mBulletDelayTickCount;
        var s = this.mBulletMoveDistanceForCalculation;
        var v0 = this.mBulletAddPosY;
        var v = x / ((y - x0 * t0) / s + x0 / v0);
        return v - v0;
    };
    //判断是否可中
    ShootDollUI.prototype.CanThroughByDefault = function () {
        var curRotation = this.mRotateCircle.rotation;
        if (curRotation < 0)
            curRotation += 360;
        var bulletCurTickCount = this.mBulletMoveDistanceForCalculation / this.mBulletAddPosY;
        var virtualRealRotation = curRotation + this.mCircleAddAngle * bulletCurTickCount;
        var realRotation = virtualRealRotation % 360;
        var bThroughCircle = false;
        var offsetRotation = 0;
        if (this.mThroughCircleMinAngle <= realRotation + offsetRotation && realRotation + offsetRotation <= this.mThroughCircleMaxAngle) {
            bThroughCircle = true;
        }
        return bThroughCircle;
    };
    ShootDollUI.prototype.ShootBullet = function () {
        this.mBullet.x = this.mBulletStartX;
        this.mBullet.y = this.mBulletStartY;
        this.mBulletTargetPosY = this.mRotateCircle.y;
        if (!this.mbShoot) {
            this.mBulletTargetPosY += this.mRotateCircle.height * .5;
        }
    };
    ShootDollUI.prototype.PlayBulletDragEffect = function () {
        if (!this.mBulletDragEffect) {
            this.mBulletDragEffect = ParticleManager.CreateParticle(this.mBullet, this.mBullet.width * .5, this.mBullet.height * .5, "shoot_drag_png", "shoot_drag_json");
        }
        this.mBulletDragEffect.start();
    };
    ShootDollUI.prototype.StopBulletDragEffect = function () {
        if (this.mBulletDragEffect) {
            this.mBulletDragEffect.stop();
        }
    };
    //开始射击
    ShootDollUI.prototype.StartShoot = function (serverData) {
        if (this.mBulletTargetPosY > 0)
            return;
        if (!this.HaveEnoughCost()) {
            UIManager.OpenUI(UIName.CoinNotEnoughUI, this.parent);
            return;
        }
        //this.ReduceCost();
        this.DisplayDiamondNum();
        //this.DisplayCostNum();
        //服务器认为可以射中，并且客户端打中时 this.mbShoot = true;
        var bCanShoot = serverData.predict_result == '1';
        egret.log("===" + serverData.predict_result);
        var bCanThrough = this.CanThroughByDefault();
        this.mbShoot = bCanShoot && bCanThrough; //最后判断能否中奖
        this.mRotationDes.text = "" + this.mbShoot; //暂未发现用途
        this.ShootBullet();
        this.PlayBulletDragEffect();
        this.RotateChange(); //
        this.TellAndCheckServerShootStatus();
    };
    ShootDollUI.prototype.HaveEnoughCost = function () {
        var bEnough = true;
        //let totalticket: number = PlayerData.GetInstance().Getticketnum();
        // let ticket = this.mShootDollData.mTicket;
        // if (ticket > 0 && totalticket >= ticket) {
        //     bEnough = true;
        // }
        // let totalDiamond = PlayerData.GetInstance().GetDiamond();
        // let price = this.mShootDollData.mPrice;
        // if (price > 0 && totalDiamond >= price) {
        //     bEnough = true;;
        // }
        return bEnough;
    };
    ShootDollUI.prototype.requestBeginGame = function () {
        var _this = this;
        //let totalticket: number = PlayerData.GetInstance().Getticketnum();
        // let ticket = this.mShootDollData.mTicket;
        // if (ticket > 0 && totalticket > 0 && totalticket >= ticket) {
        //     PlayerData.GetInstance().Setticketnum(totalticket - ticket);
        // } else {
        //     PlayerData.GetInstance().AddDiamond(-this.mShootDollData.mPrice);
        // }
        var params = { game_type: "1", id: this.mShootDollData.mDollID };
        HttpManager.SendPost(HttpManager.BEIGIN_GAME, this, params, function (data) {
            var code = data.code;
            if (code === 200) {
                var temp = data['data'];
                _this.game_log_id = temp.game_log_id;
                PlayerData.GetInstance().SetDiamond(temp.balance);
                _this.StartShoot(temp);
            }
            else {
                Toast.launch(data.descrp);
            }
        });
    };
    ShootDollUI.prototype.TellAndCheckServerShootStatus = function () {
        // let hit = "0";
        // if (this.mbShoot) hit = "1";
        // let params = { doll_id: this.mShootDollData.mDollID, tvmid: HttpManager.msTvmId, hit: hit };
        // HttpManager.SendPost(HttpManager.TVM_SHOOT_DOLL, this, params, (data) => {
        //     let code = data.code;
        //     if (code === 200) {
        //         let shoot = data.shoot_ret;
        //         //客户端自己减hp
        //         // let shootInfo = data.shoot_info;
        //         // if(shootInfo){
        //         //     let curHp = shootInfo.blood;
        //         //     this.mMonsterCurHp = parseInt(curHp);
        //         //     this.DisplayMonsterHp();
        //         // }
        //         egret.log(data.pointadd);
        //         if (data.pointadd != 0) {
        //             let msg = "元宝+" + data.pointadd;
        //             Toast.Underlaunch(msg, 50);
        //         }
        //     } else {
        //         // Toast.launch(data.descrp);
        //     }
        // });
    };
    //显示射中
    ShootDollUI.prototype.ShootDollSuccess = function () {
        var _this = this;
        //let msg = UICommon.GetShootSuccessBarrage(this.mName, this.mShootDollData.mName);
        // this.DisplayRoomBarrage(this.mAvatar, msg, this.mName);
        UIManager.OpenUI(UIName.ShootSuccessUI, this.parent, function (openUI) {
            if (openUI) {
                openUI.SetData("imageURL", _this.mShootDollData.mImg);
            }
        });
        //WorldBarrageSocket.GetInstance().EmitShoot(this.mAvatar, this.mName, msg, this.mShootDollData.mDollID, this.mShootDollData.mName);
    };
    //中奖后
    ShootDollUI.prototype.TweenShootAvatar = function (callBack) {
        egret.Tween.removeTweens(this.mShootAvatar);
        egret.Tween.get(this.mShootAvatar).to({ y: 650 }, 1000).call(function () {
            if (callBack)
                callBack();
        });
    };
    ShootDollUI.prototype.ResetShootAvatarInfo = function () {
        egret.Tween.removeTweens(this.mShootAvatar);
        this.mShootAvatar.y = this.mShootAvatarInitPosY;
    };
    ShootDollUI.prototype.reportGameResult = function (result) {
        if (Helper.StrEmptyOrNull(this.game_log_id))
            return;
        var params = { game_type: "1", game_result: result, game_log_id: this.game_log_id };
        HttpManager.SendPost(HttpManager.REPORT_GAME_RESULT, this, params, function (data) {
            var code = data.code;
            if (code === 200) {
            }
            else {
                //Toast.launch(data.descrp);
            }
        });
        this.game_log_id = "";
    };
    /**成功后 刷新新的娃娃 */
    ShootDollUI.prototype.RefreshShootDollInfo = function () {
        // let params = { doll_id: this.mShootDollData.mDollID, uid: PlayerData.GetInstance().GetID() };
        // HttpManager.SendPost(HttpManager.GET_SHOOT_CAN_HITED, this, params, (data) => {
        //     let code = data.code;
        //     if (code === 200) {
        //         this.mShootDollData.mHp = this.mShootDollData.mMaxHp;
        //         this.InitMonsterHpInfo();
        //         this.DisplayMonsterHp();
        //     } else {
        //         Toast.launch(data.descrp);
        //     }
        // });
        this.InitMonsterHpInfo();
        this.DisplayMonsterHp();
    };
    ShootDollUI.prototype.VisibleGuide = function () {
        this.mbGuide = GameData.GetInstance().GetShootGuide();
        this.mGuideGroup.visible = !this.mbGuide;
        if (!this.mbGuide)
            GameData.GetInstance().SetShootGuide();
    };
    //播放背景音乐
    ShootDollUI.prototype.PlayBgMusic = function () {
        SoundManager.PlaySoundMusic("shootBgMusic.mp3");
    };
    ShootDollUI.prototype.StopBgMusic = function () {
        SoundManager.StopSoundMusic("shootBgMusic.mp3");
    };
    ShootDollUI.prototype.PlayClickSoundEffect = function () {
        SoundManager.PlaySoundEffect("shootClick.mp3", 0, 1);
    };
    ShootDollUI.prototype.PlayHitSoundEffect = function () {
        SoundManager.PlaySoundEffect("shoot.mp3", 0, 1);
    };
    ShootDollUI.prototype.PlayHitMissSoundEffect = function () {
        SoundManager.PlaySoundEffect("shootMiss.mp3", 0, 1);
    };
    ShootDollUI.prototype.PlayWinSoundEffect = function () {
        SoundManager.PlaySoundEffect("shootWin.mp3", 0, 1);
    };
    ShootDollUI.prototype.DoJoin = function (name) {
        var _this = this;
        this.mChannel = this.agoraSession.channelJoin(name);
        this.mChannel.onChannelJoined = function () {
            _this.agoraSession.invoke("io.agora.signal.channel_query_userlist_all", { "name": _this.mDeviceID }, function (err, vallist) {
                var name = _this.HandleName(_this.mName);
                _this.EmitBarrage(_this.mAvatar, "\u3010" + name + "\u3011\u8FDB\u5165\u623F\u95F4");
                _this.SetOnlineNum(parseInt(vallist.num));
                var max = Math.min(5, vallist.list.length);
                var index = 0;
                var _loop_1 = function (i) {
                    var id = vallist.list[i][0];
                    _this.GetAudiencesAvatar(id, function (avatar) {
                        index++;
                        if (avatar) {
                            _this.PushOnlineUserArray(id, avatar);
                        }
                        else {
                            //没有头像，减去
                            _this.AddOnlineNum(-1);
                        }
                        if (index == max)
                            _this.DisplayAudienceAvatar();
                    });
                };
                for (var i = 0; i < max; i++) {
                    _loop_1(i);
                }
            });
        };
        this.mChannel.onChannelUserLeaved = function (data) {
            _this.GetAudiencesAvatar(data, function (avatar) {
                _this.AddOnlineNum(-1);
                _this.RemoveOnlineUserArray(data, avatar);
                _this.DisplayAudienceAvatar();
            });
        };
        this.mChannel.onChannelUserJoined = function (data) {
            _this.GetAudiencesAvatar(data, function (avatar) {
                _this.AddOnlineNum(+1);
                _this.PushOnlineUserArray(data, avatar);
                _this.DisplayAudienceAvatar();
            });
        };
        this.mChannel.onChannelJoinFailed = function () {
            Toast.launch("加入信令失败，请退出重试");
        };
        //频道消息回调
        this.mChannel.onMessageChannelReceive = function (account, uid, data) {
            var data = JSON.parse(data);
            switch (data.messageType) {
                case _this.NORMAL:
                    _this.OnChat(data);
                    break;
            }
        };
    };
    //获取在线头像
    ShootDollUI.prototype.GetAudiencesAvatar = function (id, callBack) {
        this.agoraSession.invoke("io.agora.signal.user_get_attr_all", { "account": id }, function (err, val) {
            var id = val.account;
            var avatar = JSON.parse(val.json).icon;
            if (callBack) {
                callBack(avatar);
            }
        });
    };
    ///发送频道消息
    ShootDollUI.prototype.EmitNewMessage = function (message, chatType) {
        var msg = JSON.stringify({
            uid: this.mID,
            remoteUid: this.mRemoteUserID,
            messageContent: message,
            userName: this.mName,
            playerAvatar: this.mAvatar,
            userAvatar: this.mAvatar,
            messageType: this.NORMAL,
            chatType: chatType
        });
        this.mChannel.messageChannelSend(msg, function () {
        });
    };
    //聊天
    ShootDollUI.prototype.EmitChat = function (msg) {
        //WorldBarrageSocket.GetInstance().Emit(this.mAvatar, this.mName, msg, this.mDeviceID, 'chat', 'shoot');
    };
    ///弹幕
    ShootDollUI.prototype.EmitBarrage = function (avatarURL, msg) {
        this.EmitNewMessage(msg, "barrage");
    };
    //获取并显示奖品详情
    ShootDollUI.prototype.RequestDollDetailInfo = function () {
        //this.mDollDetailData = new DollDetailData();
        // HttpManager.SendPost(HttpManager.GET_DOLL_DETAIL_INFO, this, { dollid: this.mShootDollData.mDollID }, (data) => {
        //     //无错误码
        //     this.mDollDetailData.mAvatarURL = this.mShootDollData.mImg;
        //     this.mDollDetailData.mName = data.name;
        //     this.mDollDetailData.mLength = data.length;
        //     this.mDollDetailData.mDes = data.des;
        //     this.mDollDetailData.mTransportInfo = data.transportInfo;
        //     this.DisplayDollDetailInfo();
        // });
        this.DisplayDollDetailInfo();
    };
    ShootDollUI.prototype.AddOnlineNum = function (addNum) {
        this.SetOnlineNum(this.mOnlineNum + addNum);
    };
    ShootDollUI.prototype.SetOnlineNum = function (num) {
        num = Math.max(0, num);
        this.mOnlineNum = num;
        this.mOnlineNumLabel.text = this.mOnlineNum + "\u4EBA";
    };
    ShootDollUI.prototype.PushOnlineUserArray = function (id, avatar) {
        this.mOnlineUserAvatarArray.push([id, avatar]);
    };
    ShootDollUI.prototype.RemoveOnlineUserArray = function (id, avatar) {
        var index = this.mOnlineUserAvatarArray.indexOf([id, avatar]);
        if (index > -1)
            this.mOnlineUserAvatarArray.splice(index, 1);
    };
    //初始化观众信息  在线人数 和前三位
    ShootDollUI.prototype.InitAudienceAvatar = function () {
        this.mAudienceAvatar.mask = this.mAudienceAvatarMask;
        this.mAudienceAvatar0.mask = this.mAudienceAvatarMask0;
        this.mAudienceAvatar1.mask = this.mAudienceAvatarMask1;
    };
    ShootDollUI.prototype.DisplayAudienceAvatar = function () {
        var avatars = [];
        for (var _i = 0, _a = this.mOnlineUserAvatarArray; _i < _a.length; _i++) {
            var v = _a[_i];
            if (v[1])
                avatars.push(v[1]);
            if (avatars.length >= 3)
                break;
        }
        if (avatars[0])
            Helper.SetImageCrossOrigin(this.mAudienceAvatar, avatars[0]);
        if (avatars[1])
            Helper.SetImageCrossOrigin(this.mAudienceAvatar0, avatars[1]);
        if (avatars[2])
            Helper.SetImageCrossOrigin(this.mAudienceAvatar1, avatars[2]);
        var lenght = avatars.length;
        this.mAudiencesInfo.right = -160 + lenght * 50;
    };
    //显示是否收藏
    ShootDollUI.prototype.DisplayStarStatus = function () {
        //this.mStarToggle.selected = PlayerData.GetInstance().IsStar(this.mDeviceID);
    };
    //显示用户余额等信息  是金币还是娃娃券
    ShootDollUI.prototype.DisplayDiamondNum = function () {
        var totalDiamond = PlayerData.GetInstance().GetDiamond();
        var strDiamond = totalDiamond > 10000 ? totalDiamond / 10000 + "\u4E07" : totalDiamond.toString();
        this.mDiamondNum.text = "\u5F53\u524D\u62E5\u6709:" + strDiamond;
        this.mDiamondIcon.x = this.mDiamondNum.textWidth + 10;
    };
    //显示需要花费多少金币 或者券
    ShootDollUI.prototype.DisplayCostNum = function () {
        //let totalticket: number = PlayerData.GetInstance().Getticketnum();
        // let ticket = this.mShootDollData.mTicket;
        // if (ticket > 0 && totalticket > 0 && totalticket >= ticket) {
        //     this.mCostNum.text = `${ticket}`;
        // } else {
        //     this.mCostNum.text = `${this.mShootDollData.mPrice}`;
        // }
        this.mCostNum.text = "" + this.mShootDollData.mPrice;
    };
    //显示谁在游戏等信息
    ShootDollUI.prototype.DisplayUsingInfo = function () {
        this.mUsingDes.text = this.HandleName(this.mName) + "\u6B63\u5728\u6E38\u620F\u4E2D";
        Helper.SetImageCrossOrigin(this.mUserHead, this.mAvatar);
        this.mUserHead.mask = this.mUserHeadMask;
    };
    ShootDollUI.prototype.OnChat = function (data) {
        // egret.log("OnChat: " + data);
        var type = data.chatType;
        var msg = data.messageContent;
        var name = data.userName;
        if (type === "chat") {
            this.AddChatContent(name, msg);
            this.RefreshChatDisplayedInfo();
        }
        else if (type === "barrage") {
            var avatarURL = data.userAvatar;
            this.DisplayRoomBarrage(avatarURL, msg, name);
        }
    };
    /**world chat */
    ShootDollUI.prototype.WorldOnChat = function (name, msg) {
        this.AddChatContent(name, msg);
        this.RefreshChatDisplayedInfo();
    };
    ShootDollUI.prototype.AddChatContent = function (name, content) {
        name = this.HandleName(name);
        this.mChatTextFlow.push({ text: (this.mChatTextFlow.length > 0 ? '\n' : '') + (name + ": "), style: { "textColor": 0xffff70 } });
        this.mChatTextFlow.push({ text: "" + content, style: { "textColor": 0xffffff } });
        if (this.mChatTextFlow.length > this.mChatMaxCount * 2) {
            this.mChatTextFlow.shift();
            this.mChatTextFlow.shift();
        }
    };
    //聊天信息显示收藏控制
    ShootDollUI.prototype.RefreshChatDisplayedInfo = function () {
        var bVisible = !this.mChatVisibleToggle.selected;
        this.mChatContent.visible = bVisible;
        this.mChatContentBg.visible = bVisible;
        if (!bVisible)
            return;
        this.mChatContent.textFlow = this.mChatTextFlow;
        this.mChatContentBg.height = Math.min(this.mChatScroller.height, this.mChatContent.textHeight) + 40;
    };
    //聊天助手显示隐藏
    ShootDollUI.prototype.VisibleChatHandledInfo = function (visible) {
        this.mChatHandledInfo.visible = visible;
    };
    ShootDollUI.prototype.InitChatPhraseInfo = function () {
        var dataArray = [];
        dataArray.push({ text: "666" });
        dataArray.push({ text: "2333" });
        dataArray.push({ text: "厉害了我的哥" });
        dataArray.push({ text: "你好菜呀" });
        dataArray.push({ text: "放开那个娃娃，让我来" });
        dataArray.push({ text: "扎心了，老铁" });
        this.mChatPhraseList.dataProvider = new eui.ArrayCollection(dataArray);
        this.mChatPhraseList.itemRendererSkinName = "CharPhraseIRSkin";
        this.mChatPhraseList.addEventListener(eui.ItemTapEvent.ITEM_TAP, this.ClickChatPhraseItem, this);
    };
    ShootDollUI.prototype.ClickChatPhraseItem = function (evt) {
        var msg = this.mChatPhraseList.selectedItem.text;
        this.EmitChat(msg);
        this.VisibleChatHandledInfo(false);
    };
    ShootDollUI.prototype.VisibleChatPhraseInfo = function () {
        this.mChatPhraseInfo.visible = !this.mChatPhraseVisibleToggle.selected;
    };
    ShootDollUI.prototype.GetShootBarrage = function (name) {
        name = this.HandleName(name);
        var barrage = "";
        var arry = [];
        arry.push("\u3010" + name + "\u3011\u5C04\u4E2D\u5B88\u62A4\u517D\u5566\uFF0C\u52A0\u6CB9\uFF0C\u80DC\u5229\u5728\u671B");
        arry.push("\u5B88\u62A4\u517D\uFF1A\u4E3A\u4EC0\u4E48\u3010" + name + "\u3011\u4F60\u8981\u6253\u6211");
        var randomNum = Helper.Random(0, arry.length);
        return arry[randomNum];
    };
    ShootDollUI.prototype.GetNotShootBarrage = function (name) {
        name = this.HandleName(name);
        var barrage = "";
        var arry = [];
        arry.push("\u771F\u53EF\u60DC\uFF0C\u3010" + name + "\u3011\u5DEE\u70B9\u5C31\u5C04\u4E2D\u4E86~");
        arry.push("\u6211\u52D2\u4E2A\u53BB\uFF0C\u3010" + name + "\u3011\u8FD9\u6837\u90FD\u6CA1\u5C04\u4E2D~");
        var randomNum = Helper.Random(0, arry.length);
        return arry[randomNum];
    };
    ShootDollUI.prototype.HandleName = function (name) {
        return UICommon.HandleName(name);
    };
    ShootDollUI.prototype.DisplayRoomBarrage = function (avatarURL, msg, name) {
        // RoomBarrageManager.GetInstance().Lanuch(avatarURL, msg, name);
    };
    //显示空闲   同类型机器
    ShootDollUI.prototype.DisplaySameInfo = function (sameData) {
        // let freeCount = 0;
        // let totalCount = 0;
        // let data: Array<Object> = [];
        if (sameData === void 0) { sameData = null; }
        // if (sameData) {
        //     let freeStatusType = 2;
        //     for (let v of sameData) {
        //         let status = v.channel_status;
        //         if (status == freeStatusType) {
        //             freeCount++;
        //         }
        //         totalCount++;
        //         //data.push({ id: v.id, icon: this.mShootDollData.mImg, status: v.channel_status });
        //     }
        // }
        // this.mSameFreeCount.text = freeCount.toString();
        // this.mSameTotalCount.text = totalCount.toString();
        // this.mSameList.dataProvider = new eui.ArrayCollection(data);
        // this.mSameList.itemRenderer = SameItemIRUI;
    };
    ShootDollUI.prototype.ClickSameItem = function (item) {
        //let id = this.mSameList.selectedItem.id;
        // UICommon.AutoOpenVideoDisplayUIByDeviceID(id, this);
    };
    ShootDollUI.prototype.SameBtn = function (evt) {
        // let min = 0;
        // let max = this.mSameScroller.viewport.contentWidth - this.mSameScroller.width;
        // if (max <= min) return;
        // let target = evt.currentTarget as eui.Button;
        // let moveDistance = -110;
        // if (target === this.mSameLeftBtn) {
        //     moveDistance = 110;
        // }
        // let scrollH = this.mSameScroller.viewport.scrollH;
        // scrollH = Math.min(max, Math.max(min, scrollH + moveDistance));
        // this.mSameScroller.viewport.scrollH = scrollH;
    };
    //显示娃娃详情 点击切换 房间记录
    ShootDollUI.prototype.VisibleDollDetailInfo = function (visible) {
        this.mDollDetailGroup.visible = visible;
    };
    //显示娃娃详情 
    ShootDollUI.prototype.DisplayDollDetailInfo = function () {
        Helper.SetImageCrossOrigin(this.mDollAvatar, this.mShootDollData.mImg);
        //Helper.ImageAddMask(this.mDollAvatar);
        this.mDollName.text = "\u540D\u79F0\uFF1A" + this.mShootDollData.mName;
        this.mDollDes.text = "\u63CF\u8FF0\uFF1A" + this.mShootDollData.mDes;
        this.mDollTransportInfo.text = "\u53D1\u8D27\u65B9\u5F0F\uFF1A" + this.mShootDollData.mTransportInfo;
    };
    //房间记录
    ShootDollUI.prototype.VisibleRecordGroup = function (visible) {
        this.mRecordGroup.visible = visible;
    };
    ShootDollUI.prototype.RequestRecords = function () {
        this.mRecordList.dataProvider = null;
        // let params = { doll_id: this.mShootDollData.mDollID };
        // HttpManager.SendPost(HttpManager.GET_SHOOT_DOLL_RECORD, this, params, (data) => {
        //     let code = data.code;
        //     if (code === 200) {
        //         let dataList: Array<Object> = [];
        //         for (let v of data.doll_data) {
        //             let userName = v.user_login;
        //             dataList.push({ icon: v.avatar, name: userName, time: v.shoot_time });
        //         }
        //         this.mRecordList.dataProvider = new eui.ArrayCollection(dataList);
        //         this.mRecordList.itemRenderer = RecordUserListItem;
        //     }
        //     else {
        //         Toast.launch(data.descrp);
        //     }
        // });
    };
    ShootDollUI.prototype.PlayeEffect = function () {
        if (!this.particle2)
            this.particle2 = ParticleManager.CreateParticle(this, 318, 0, "cheng_png", "cheng_json");
        if (!this.particle3)
            this.particle3 = ParticleManager.CreateParticle(this, 318, 50, "dian_png", "dian_json");
        if (!this.particle4)
            this.particle4 = ParticleManager.CreateParticle(this, 318, 0, "hong_png", "hong_json");
        if (!this.particle5)
            this.particle5 = ParticleManager.CreateParticle(this, 318, 0, "huang_png", "huang_json");
        if (!this.particle6)
            this.particle6 = ParticleManager.CreateParticle(this, 318, 0, "lan_png", "lan_json");
        if (!this.particle7)
            this.particle7 = ParticleManager.CreateParticle(this, 318, 0, "lv_png", "lv_json");
        if (!this.particle8)
            this.particle8 = ParticleManager.CreateParticle(this, 318, 0, "qing_png", "qing_json");
        if (!this.particle9)
            this.particle9 = ParticleManager.CreateParticle(this, 318, 50, "xx_png", "xx_json");
        if (!this.particle10)
            this.particle10 = ParticleManager.CreateParticle(this, 318, 0, "zi_png", "zi_json");
        this.particle2.start(1000);
        this.particle3.start(1000);
        this.particle4.start(1000);
        this.particle5.start(1000);
        this.particle6.start(1000);
        this.particle7.start(1000);
        this.particle8.start(1000);
        this.particle9.start(1000);
        this.particle10.start(1000);
    };
    ShootDollUI.prototype.StopPlayEffect = function () {
        if (this.particle2)
            this.particle2.stop();
        if (this.particle3)
            this.particle3.stop();
        if (this.particle4)
            this.particle4.stop();
        if (this.particle5)
            this.particle5.stop();
        if (this.particle6)
            this.particle6.stop();
        if (this.particle7)
            this.particle7.stop();
        if (this.particle8)
            this.particle8.stop();
        if (this.particle9)
            this.particle9.stop();
        if (this.particle10)
            this.particle10.stop();
    };
    //=============================button method====================================
    ShootDollUI.prototype.BackBtn = function () {
        // HttpManager.RefreshTVMToken(() => {
        //     UIManager.OpenUI(UIName.MainUI, this.parent);
        //     UIManager.OpenUI(UIName.FooterUI, this.parent);
        //     this.StopPlayEffect();
        //     super.BackBtn();
        // });
        this.StopPlayEffect();
        _super.prototype.BackBtn.call(this);
    };
    ShootDollUI.prototype.ChatBtn = function () {
        //Toast.launch("123");
        // this.VisibleChatHandledInfo(true);
        // this.mChatInput.text = "";
        // if (!this.mChatPhraseVisibleToggle.selected) {
        //     this.InitChatPhraseInfo();
        // }
        window["shareFriend"]();
    };
    ShootDollUI.prototype.ChatVisibleToggle = function () {
        this.RefreshChatDisplayedInfo();
    };
    ShootDollUI.prototype.ChatSendBtn = function () {
        var msg = this.mChatInput.text;
        if (Helper.StrEmptyOrNull(msg) || msg === this.mChatInput.prompt) {
            Toast.launch("请输入聊天内容");
            return;
        }
        this.EmitChat(msg);
        this.VisibleChatHandledInfo(false);
    };
    ShootDollUI.prototype.CloseChatHandleBtn = function () {
        this.VisibleChatHandledInfo(false);
    };
    ShootDollUI.prototype.ChatPhraseVisibleToggle = function () {
        this.VisibleChatPhraseInfo();
    };
    ShootDollUI.prototype.PayBtn = function () {
        // if (GameData.GetInstance().IsTVM()) {
        //     window["OpenSystemRecharge"]();
        // } else {
        //     let FooterUI = UIManager.GetUIObj(UIName.FooterUI) as FooterUI;
        //     if (FooterUI) {
        //         FooterUI.ClickFooterRadioByType(eFooterType.Pay);
        //     }
        // }
        window["test"]();
        //Toast.launch("chongzhi");
    };
    ShootDollUI.prototype.DollDetailBtn = function () {
        this.VisibleRecordGroup(false);
        this.VisibleDollDetailInfo(true);
    };
    //收藏
    ShootDollUI.prototype.StarToggle = function () {
        //let bStar = this.mStarToggle.selected;
        // PlayerData.GetInstance().ModifyStarList(this.mDeviceID.toString(), bStar);
        // let params = { uid: PlayerData.GetInstance().GetID(), setting: PlayerData.GetInstance().GetStrStarList() };
        //HttpManager.SendPost(HttpManager.SET_USER_SETTING, this, params, null);
    };
    //射击按钮
    ShootDollUI.prototype.ShootBtn = function () {
        // if (!this.mbGuide) {
        //     this.CloseGuide();
        //     return;
        // }
        this.PlayClickSoundEffect();
        //let params = { doll_id: this.mShootDollData.mDollID, uid: PlayerData.GetInstance().GetID() };
        //let data = {hit:"1"};
        this.requestBeginGame();
        //this.StartShoot(data);
        // HttpManager.SendPost(HttpManager.GET_SHOOT_CAN_HITED, this, params, (data) => {
        //     let code = data.code;
        //     if (code === 200) {
        //         this.StartShoot(data);
        //     } else {
        //         Toast.launch(data.descrp);
        //     }
        // });
    };
    ShootDollUI.prototype.RecordBtn = function () {
        this.VisibleRecordGroup(true);
        this.RequestRecords();
        this.VisibleDollDetailInfo(false);
    };
    ShootDollUI.prototype.OpenShare = function () {
        UIManager.OpenUI(UIName.SharePhotoUI, this.parent);
    };
    ShootDollUI.prototype.CloseGuide = function () {
        this.mGuideGroup.visible = false;
        this.mbGuide = true;
    };
    //显示再射几次  获取精美礼品
    ShootDollUI.prototype.GiftTween = function () {
        // let params = { uid: PlayerData.GetInstance().GetID() };
        // HttpManager.SendPost(HttpManager.GET_SHOOT_GIFTTASK_API, this, params, (data) => {
        //     if (data.code == 200) {
        //         PlayerData.GetInstance().shoottaskState = data.taskstate;
        //         PlayerData.GetInstance().shoottaskcurCatchNum = 0;
        //         PlayerData.GetInstance().shoottasktotalCatchNum = 0;
        //         PlayerData.GetInstance().shoottaskTime = 0;
        //         this.priceImage.visible = false;
        //         this.bg.visible = true;
        //         this.bg2.visible = false;
        //         this.itemicon2.visible = true;
        //         this.itemicon.visible = false;
        //         egret.Tween.removeTweens(this.shootGiftGroup);
        //         if (data.taskstate === 0)///0：没任务   1:未完成，进行中  2：已完成可领取   
        //         {
        //             this.shootGiftGroup.visible = false;
        //         } else if (data.taskstate === 1) {
        //             this.shootGiftGroup.visible = true;
        //             PlayerData.GetInstance().shoottaskcurCatchNum = Number(data.step);
        //             PlayerData.GetInstance().shoottasktotalCatchNum = Number(data.stepneed);
        //             PlayerData.GetInstance().shoottaskTime = Number(data.time);
        //             this.needShoot.text = ((PlayerData.GetInstance().shoottasktotalCatchNum - PlayerData.GetInstance().shoottaskcurCatchNum)).toString();
        //             this.giftDes.text = "";
        //             this.giftName.text = "精美礼品";
        //             this.giftPrice.text = "";
        //             let tempxPos: number = this.shootGiftGroup.x;
        //             egret.Tween.get(this.shootGiftGroup).to({ "x": 10 }, 400).to({ "x": 10 }, 800).to({ "x": -200 }, 400);
        //         } else if (data.taskstate === 2) {
        //             this.shootGiftGroup.visible = true;
        //             this.bg.visible = false;
        //             this.bg2.visible = true;
        //             this.itemicon2.visible = false;
        //             this.itemicon.visible = true;
        //             let params = { uid: PlayerData.GetInstance().GetID() };
        //             HttpManager.SendPost(HttpManager.GET_SHOOT_TASKGIFT_TASK_AWARD, this, params, (data) => {
        //                 if (data.code == 200) {
        //                     this.priceImage.visible = true;
        //                     PlayerData.GetInstance().shoottaskTime = Number(data.time);
        //                     this.needShoot.text = "";
        //                     this.giftDes.text = "";
        //                     this.giftName.text = data.giftinfo["name"];
        //                     this.giftPrice.text = "价值约¥" + data.giftinfo["price_src"];
        //                     let temp = data.giftinfo["pic"];
        //                     Helper.SetImageCrossOrigin(this.itemicon, temp);
        //                     HttpManager.RefreshTVMToken(() => {
        //                         this.DisplayDiamondNum();
        //                     });
        //                     let tempxPos: number = this.shootGiftGroup.x;
        //                     egret.Tween.get(this.shootGiftGroup).to({ "x": 10 }, 400).to({ "x": 10 }, 800).to({ "x": -200 }, 400);
        //                 } else {
        //                 }
        //             });
        //         }
        //     }
        // });
    };
    //显示恢复一滴血倒计时
    ShootDollUI.prototype.ShowRecoveryHP = function (countdown) {
        if (countdown > 0) {
            this.mRecoveryHP_Time.visible = true;
            this.mRecoveryHP_Text.visible = true;
            this.mRecoveryHP_Time.text = countdown.toString();
        }
        else {
            this.mRecoveryHP_Time.visible = false;
            this.mRecoveryHP_Text.visible = false;
        }
    };
    return ShootDollUI;
}(BaseUI));
__reflect(ShootDollUI.prototype, "ShootDollUI");
//血量item
var MonsterHpIRUI = (function (_super) {
    __extends(MonsterHpIRUI, _super);
    function MonsterHpIRUI() {
        var _this = _super.call(this) || this;
        _this.skinName = "MonsterHpIRSKin";
        return _this;
    }
    MonsterHpIRUI.prototype.dataChanged = function () {
        this.mEmptyHp.visible = this.data.empty;
    };
    return MonsterHpIRUI;
}(eui.ItemRenderer));
__reflect(MonsterHpIRUI.prototype, "MonsterHpIRUI");
//# sourceMappingURL=ShootDollUI.js.map