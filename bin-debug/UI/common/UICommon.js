var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var UICommon = (function () {
    function UICommon() {
    }
    UICommon.GetShootDeviceIDByDollID = function (dollID) {
        return 1000000 + dollID;
    };
    UICommon.HandleName = function (name) {
        if (name.length > 2) {
            name = name.slice(0, 5) + "...";
        }
        return name;
    };
    return UICommon;
}());
__reflect(UICommon.prototype, "UICommon");
//# sourceMappingURL=UICommon.js.map