var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var UIManager = (function () {
    function UIManager() {
    }
    UIManager.GetUIObj = function (name) {
        var uiObj = null;
        for (var _i = 0, _a = this.msUIObjArray; _i < _a.length; _i++) {
            var v = _a[_i];
            if (v["name"] === name) {
                uiObj = v["uiObj"];
                break;
            }
        }
        if (!uiObj) {
            uiObj = this.NewUIObj(name);
            if (uiObj) {
                this.msUIObjArray.push({ name: name, uiObj: uiObj });
            }
            else {
                console.log("classUI is null >>> " + name);
            }
        }
        return uiObj;
    };
    UIManager.OpenUI = function (name, parent, setDataFunc) {
        if (!parent)
            return;
        var openUI = this.GetUIObj(name);
        if (setDataFunc) {
            setDataFunc(openUI);
        }
        parent.addChild(openUI);
        openUI.Open();
        return openUI;
    };
    UIManager.CloseUIByName = function (name) {
        var uiObj = null;
        for (var _i = 0, _a = this.msUIObjArray; _i < _a.length; _i++) {
            var l = _a[_i];
            if (l["name"] === name) {
                uiObj = l["ClassUI"];
                break;
            }
        }
        if (uiObj)
            this.CloseUI(uiObj);
    };
    UIManager.CloseUI = function (uiObj) {
        if (!uiObj || !uiObj.mbIsOpen)
            return;
        uiObj.Close();
        if (uiObj.parent) {
            uiObj.parent.removeChild(uiObj);
        }
    };
    UIManager.NewUIObj = function (name) {
        var clazz = egret.getDefinitionByName(name);
        return new clazz();
    };
    UIManager.msUIObjArray = [];
    return UIManager;
}());
__reflect(UIManager.prototype, "UIManager");
//# sourceMappingURL=UIManager.js.map