var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var DefineUI = (function () {
    function DefineUI() {
    }
    return DefineUI;
}());
__reflect(DefineUI.prototype, "DefineUI");
var mcCustomSkinsPath = "resource/custom_skins/";
var UIName = (function () {
    function UIName() {
    }
    UIName.DiamondRecordUI = "DiamondRecordUI";
    UIName.EditAddressUI = "EditAddressUI";
    UIName.FeedbackUI = "FeedbackUI";
    UIName.GiftCodeInputUI = "GiftCodeInputUI";
    UIName.GiftCodeUI = "GiftCodeUI";
    UIName.GrabRecordUI = "GrabRecordUI";
    UIName.NotifyUI = "NotifyUI";
    UIName.LoginUI = "LoginUI";
    UIName.PayUI = "PayUI";
    UIName.QuestionsUI = "QuestionsUI";
    UIName.AboutUI = "AboutUI";
    UIName.FooterUI = "FooterUI";
    UIName.MainUI = "MainUI";
    UIName.SettingUI = "SettingUI";
    UIName.PlayerInfoUI = "PlayerInfoUI";
    UIName.VideoDisplayUI = "VideoDisplayUI";
    UIName.GrabDollUI = "GrabDollUI";
    UIName.RecordListUI = "RecordListUI";
    UIName.FailTryAgainUI = "FailTryAgainUI";
    UIName.SignInUI = "SignInUI";
    UIName.SucessGrabDoll = "SucessGrabDoll";
    UIName.DollDetailUI = "DollDetailUI";
    UIName.ShopUI = "ShopUI";
    UIName.GoodsInfoUI = "GoodsInfoUI";
    UIName.ExchangeRecordUI = "ExchangeRecordUI";
    UIName.ExchangeBigDollUI = "ExchangeBigDollUI";
    UIName.ExchangeBigDollRecordUI = "ExchangeBigDollRecordUI";
    UIName.ExchangeBigDollDetailUI = "ExchangeBigDollDetailUI";
    UIName.TransportSkinUI = "ExchangeRecordUI";
    UIName.ShootDollUI = "ShootDollUI";
    UIName.ShootRecordUI = "ShootRecordUI";
    UIName.ShootSuccessUI = "ShootSuccessUI";
    UIName.ShopUI1 = "ShopUI1";
    UIName.ShopUI2 = "ShopUI2";
    UIName.ShopUI3 = "ShopUI3";
    UIName.ShopUI4 = "ShopUI4";
    UIName.ShopUI5 = "ShopUI5";
    UIName.GuideUI = "GuideUI";
    UIName.BannerUI = "BannerUI";
    UIName.CoinNotEnoughUI = "CoinNotEnoughUI";
    UIName.ScoreInfoUI = "ScoreInfoUI";
    UIName.GetGiftUI = "GetGiftUI";
    UIName.postageUI = "postageUI";
    UIName.BuyDollUI = "BuyDollUI";
    UIName.WeekMemberUI = "WeekMemberUI";
    UIName.CouponUI = "CouponUI";
    UIName.RrewardFristUISkin = "RrewardFristUISkin";
    UIName.GiftInfoUI = "GiftInfoUI";
    UIName.ShootGiftInfoUI = "ShootGiftInfoUI";
    UIName.DaShenSkinUI = "DaShenSkinUI";
    UIName.StartGameTipUI = "StartGameTipUI";
    UIName.GiftRandomInfo = "GiftRandomUIInfo";
    UIName.ShootGiftRandomInfo = "ShootGiftRandomInfo";
    UIName.SharePhotoUI = "SharePhotoUI";
    UIName.GameHallUI = "GameHallUI";
    UIName.MainShootUI = "MainShootUI";
    UIName.MainGrabUI = "MainGrabUI";
    UIName.IndexUI = "IndexUI";
    return UIName;
}());
__reflect(UIName.prototype, "UIName");
var ePayType;
(function (ePayType) {
    ePayType[ePayType["AliPay"] = 1] = "AliPay";
    ePayType[ePayType["WeChatAppPay"] = 2] = "WeChatAppPay";
    ePayType[ePayType["WeChatWebPay"] = 12] = "WeChatWebPay";
})(ePayType || (ePayType = {}));
var eFooterType;
(function (eFooterType) {
    eFooterType[eFooterType["Doll"] = 0] = "Doll";
    eFooterType[eFooterType["Pay"] = 1] = "Pay";
    eFooterType[eFooterType["Hall"] = 2] = "Hall";
    eFooterType[eFooterType["PlayerInfo"] = 3] = "PlayerInfo";
    eFooterType[eFooterType["Shop"] = 4] = "Shop";
})(eFooterType || (eFooterType = {}));
var eTransportType;
(function (eTransportType) {
    eTransportType[eTransportType["All"] = -1] = "All";
    eTransportType[eTransportType["Waitting"] = 0] = "Waitting";
    eTransportType[eTransportType["Going"] = 1] = "Going";
    eTransportType[eTransportType["Arrived"] = 2] = "Arrived";
})(eTransportType || (eTransportType = {}));
var OpenIndex = (function () {
    function OpenIndex() {
    }
    OpenIndex.OpenIndex = -1;
    return OpenIndex;
}());
__reflect(OpenIndex.prototype, "OpenIndex");
//# sourceMappingURL=DefineUI.js.map