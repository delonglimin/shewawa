var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var BaseUI = (function (_super) {
    __extends(BaseUI, _super);
    function BaseUI() {
        var _this = _super.call(this) || this;
        _this.mbIsOpen = false;
        _this.mData = {};
        // this.addEventListener(eui.UIEvent.ADDED_TO_STAGE, this.Open, this);
        _this.skinName = mcCustomSkinsPath + _this.GetExmlName();
        return _this;
    }
    BaseUI.prototype.Open = function () {
        //egret.log('OpenUI >>> ' + this.GetExmlName());
        this.RegisterBtnEvt();
        this.RegisterGameEvt();
        this.mbIsOpen = true;
        this.width = this.stage.stageWidth;
        this.height = this.stage.stageHeight;
    };
    BaseUI.prototype.Close = function () {
        this.mbIsOpen = false;
        this.RemoveBtnEvt();
        this.RemoveGameEvt();
        this.mData = {};
        this.RemoveCurrentChildUI();
    };
    BaseUI.prototype.RegisterBtnEvt = function () { };
    BaseUI.prototype.RemoveBtnEvt = function () { };
    BaseUI.prototype.RegisterGameEvt = function () { };
    BaseUI.prototype.RemoveGameEvt = function () { };
    BaseUI.prototype.SetData = function (key, value) {
        this.mData[key] = value;
    };
    BaseUI.prototype.GetData = function (key) {
        var tempObj = this.mData[key];
        if (!tempObj) {
            egret.log(this.GetExmlName() + "; GetData(" + key + "): undefined");
        }
        ;
        return tempObj;
    };
    BaseUI.prototype.BackBtn = function () {
        UIManager.CloseUI(this);
    };
    BaseUI.prototype.SetCurrentChildUI = function (childrenUI) {
        this.mCurrentChildUI = childrenUI;
    };
    BaseUI.prototype.RemoveCurrentChildUI = function () {
        if (this.mCurrentChildUI) {
            UIManager.CloseUI(this.mCurrentChildUI);
            this.mCurrentChildUI = null;
        }
    };
    return BaseUI;
}(eui.Component));
__reflect(BaseUI.prototype, "BaseUI");
//# sourceMappingURL=BaseUI.js.map