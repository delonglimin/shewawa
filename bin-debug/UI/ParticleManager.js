var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var ParticleManager = (function () {
    function ParticleManager() {
    }
    ParticleManager.CreateParticle = function (parent, particleX, particleY, particlePng, particleJson) {
        var curParticle = new particle.GravityParticleSystem(RES.getRes(particlePng), RES.getRes(particleJson));
        curParticle.x = particleX;
        curParticle.y = particleY;
        curParticle.emitterX = 0;
        curParticle.emitterY = 0;
        curParticle.touchEnabled = false;
        parent.addChild(curParticle);
        return curParticle;
    };
    return ParticleManager;
}());
__reflect(ParticleManager.prototype, "ParticleManager");
//# sourceMappingURL=ParticleManager.js.map