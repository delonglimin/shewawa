var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var ShootSuccessUI = (function (_super) {
    __extends(ShootSuccessUI, _super);
    function ShootSuccessUI() {
        var _this = _super.call(this) || this;
        _this.countDownValue = 10;
        return _this;
    }
    ShootSuccessUI.prototype.GetExmlName = function () { return "ShootSuccessSkin.exml"; };
    ShootSuccessUI.prototype.RegisterBtnEvt = function () {
        this.mCloseBtn.once(egret.TouchEvent.TOUCH_TAP, this.CloseBtn, this);
    };
    ShootSuccessUI.prototype.CloseBtn = function () {
        this.CloseParticle();
        this.BackBtn();
    };
    ShootSuccessUI.prototype.Open = function () {
        _super.prototype.Open.call(this);
        var imageurl = this.GetData("imageURL");
        if (imageurl) {
            Helper.SetImageCrossOrigin(this.mIcon, imageurl.toString());
            Helper.ImageAddMask(this.mIcon);
        }
        this.countDownValue = 10;
        this.timer = new egret.Timer(1000, 10);
        this.timer.addEventListener(egret.TimerEvent.TIMER, this.TimeCountDown, this);
        this.timer.start();
        //this.particle1 =  ParticleManager.CreateParticle(this.parent,318,500,"bg2_png","bg_json");
        this.particle2 = ParticleManager.CreateParticle(this, 318, 0, "cheng_png", "cheng_json");
        this.particle3 = ParticleManager.CreateParticle(this, 318, 50, "dian_png", "dian_json");
        this.particle4 = ParticleManager.CreateParticle(this, 318, 0, "hong_png", "hong_json");
        this.particle5 = ParticleManager.CreateParticle(this, 318, 0, "huang_png", "huang_json");
        this.particle6 = ParticleManager.CreateParticle(this, 318, 0, "lan_png", "lan_json");
        this.particle7 = ParticleManager.CreateParticle(this, 318, 0, "lv_png", "lv_json");
        this.particle8 = ParticleManager.CreateParticle(this, 318, 0, "qing_png", "qing_json");
        this.particle9 = ParticleManager.CreateParticle(this, 318, 50, "xx_png", "xx_json");
        this.particle10 = ParticleManager.CreateParticle(this, 318, 0, "zi_png", "zi_json");
        //this.particle1.start();
        this.particle2.start();
        this.particle3.start();
        this.particle4.start();
        this.particle5.start();
        this.particle6.start();
        this.particle7.start();
        this.particle8.start();
        this.particle9.start();
        this.particle10.start();
    };
    ShootSuccessUI.prototype.TimeCountDown = function () {
        this.countDownValue -= 1;
        if (this.countDownValue === 0) {
            this.CloseParticle();
            this.BackBtn();
        }
    };
    ShootSuccessUI.prototype.CloseParticle = function () {
        this.timer.stop();
        //this.particle1.stop();
        this.particle2.stop();
        this.particle3.stop();
        this.particle4.stop();
        this.particle5.stop();
        this.particle6.stop();
        this.particle7.stop();
        this.particle8.stop();
        this.particle9.stop();
        this.particle10.stop();
    };
    return ShootSuccessUI;
}(BaseUI));
__reflect(ShootSuccessUI.prototype, "ShootSuccessUI");
//# sourceMappingURL=ShootSuccessUI.js.map