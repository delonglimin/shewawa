var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var HttpManager = (function () {
    function HttpManager() {
    }
    HttpManager.SendPost = function (methodURL, thisObj, params, complete, onGetProgress, onIOError, needToken) {
        if (needToken === void 0) { needToken = true; }
        var request = new egret.HttpRequest();
        request.responseType = egret.HttpResponseType.TEXT;
        var queryUrl = HttpManager.URL + methodURL;
        //request.open(HttpManager.URL + methodURL, egret.HttpMethod.POST);
        request.open(queryUrl, egret.HttpMethod.POST);
        request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
        if (params) {
            var strParams = needToken ? "token=" + this.msToken + "&" : "";
            for (var key in params) {
                strParams += key + "=" + params[key] + "&";
            }
            strParams = strParams.substring(0, strParams.length - 1);
            request.send(strParams);
        }
        else {
            if (needToken)
                request.send("token=" + this.msToken);
            else
                request.send();
        }
        if (complete) {
            request.addEventListener(egret.Event.COMPLETE, function (evt) {
                var request = evt.currentTarget;
                if (request.response) {
                    var data = null;
                    try {
                        data = JSON.parse(request.response);
                    }
                    catch (e) {
                        Toast.launch(methodURL + " JSON解析错误: " + request.response);
                    }
                    if (data)
                        complete(data);
                }
            }, thisObj);
        }
        if (onGetProgress) {
            request.addEventListener(egret.ProgressEvent.PROGRESS, onGetProgress, thisObj);
        }
        request.addEventListener(egret.IOErrorEvent.IO_ERROR, function (evt) {
            console.log("get IO_ERROR: " + methodURL);
            if (onIOError)
                onIOError(evt);
        }, thisObj);
    };
    HttpManager.URL = "http://dollv2.deerlive.com/"; //weChat正式地址
    HttpManager.cdnURL = "http://dollv2.deerlive.com/";
    HttpManager.DEFAULT_URL = "Api/SiSi/";
    HttpManager.GET_USERINFO = HttpManager.DEFAULT_URL + "getUserInfo";
    HttpManager.GET_SHOOT_DOLL = "Api/Shoot/getShootList";
    HttpManager.ENTER_SHOOL_DOLL = "/Api/Shoot/enterShootDoll";
    HttpManager.BEIGIN_GAME = "/Api/Shoot/beginGame";
    HttpManager.REPORT_GAME_RESULT = "/Api/Shoot/reportGameResult";
    HttpManager.msToken = "";
    HttpManager.msTvmId = "";
    return HttpManager;
}());
__reflect(HttpManager.prototype, "HttpManager");
//# sourceMappingURL=HttpManager.js.map