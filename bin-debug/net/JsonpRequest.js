var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var JsonpRequest = (function () {
    function JsonpRequest() {
    }
    JsonpRequest.Process = function (url, callback, callObj) {
        this.msCompleteCall["call_" + this.msRequestID] = callback.bind(callObj);
        // console.log(this.msCompleteCall);
        // console.log(JsonpRequest.msCompleteCall);
        this.StartLoder(url, this.msRequestID++);
    };
    JsonpRequest.StartLoder = function (url, id) {
        var script = document.createElement('script');
        script.src = url + ("JsonpRequest.msCompleteCall.call_" + id) + "";
        document.body.appendChild(script);
    };
    JsonpRequest.Start = function (method, callback, callObj, params) {
        var url = this.msURL + method + "?";
        if (params)
            url += params + "&";
        url += this.msServerJsonpCallBack + "=";
        this.Process(url, callback, callObj);
    };
    JsonpRequest.msRequestID = 0;
    JsonpRequest.msCompleteCall = {};
    JsonpRequest.msURL = "http://210.73.214.214/Api/SiSi/";
    JsonpRequest.msServerJsonpCallBack = "callback";
    return JsonpRequest;
}());
__reflect(JsonpRequest.prototype, "JsonpRequest");
//# sourceMappingURL=JsonpRequest.js.map