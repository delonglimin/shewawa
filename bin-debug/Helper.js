var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var Helper = (function () {
    function Helper() {
    }
    /** 不包括max*/
    Helper.Random = function (min, max) {
        if (min == max)
            return max;
        var randomNum = Math.random() * (Math.abs(max - min)) + (max > min ? min : max);
        return Math.floor(randomNum);
    };
    /** 包括max*/
    Helper.RandomContainMax = function (min, max) {
        if (min == max)
            return max;
        return this.Random(min, max + 1);
    };
    Helper.StrEmptyOrNull = function (str) {
        return str == null || str == "";
    };
    Helper.SetImageCrossOrigin = function (img, url) {
        if (url == "default.png" || url == HttpManager.URL + "default.png") {
            img.source = "default_avtar_png";
            return;
        }
        var flag = url.slice(0, 4);
        if (flag != "http") {
            url = HttpManager.cdnURL + url;
        }
        var imageLoader = new egret.ImageLoader();
        imageLoader.crossOrigin = "anonymous";
        imageLoader.addEventListener(egret.Event.COMPLETE, function () { img.bitmapData = imageLoader.data; }, this);
        try {
            imageLoader.load(url);
        }
        catch (e) {
            Toast.launch("imageLoader load error: " + url);
            img.source = "default_avtar_png";
        }
    };
    /**娃娃图片 添加圆角遮罩 */
    Helper.ImageAddMask = function (img) {
        if (!img || img.mask)
            return;
        var texture = RES.getRes('bg_3_png');
        var textureHeight = texture.textureHeight;
        var textureWidth = texture.textureWidth;
        var space = 2;
        var mask = new eui.Image(texture);
        mask.scale9Grid = new egret.Rectangle((textureWidth - space) * .5, (textureHeight - space) * .5, space, space);
        mask.height = img.height;
        mask.width = img.width;
        img.parent.addChild(mask);
        mask.top = img.top;
        mask.bottom = img.bottom;
        mask.right = img.right;
        mask.left = img.left;
        mask.verticalCenter = img.verticalCenter;
        mask.horizontalCenter = img.horizontalCenter;
        mask.x = img.x;
        mask.y = img.y;
        img.mask = mask;
    };
    return Helper;
}());
__reflect(Helper.prototype, "Helper");
//# sourceMappingURL=Helper.js.map