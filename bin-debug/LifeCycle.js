var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var LifeCycle = (function () {
    function LifeCycle() {
    }
    LifeCycle.Init = function () {
        egret.lifecycle.addLifecycleListener(function (context) {
            // custom lifecycle plugin
        });
        egret.lifecycle.onPause = function () {
            var shootDollUI = UIManager.GetUIObj(UIName.ShootDollUI);
            if (shootDollUI && shootDollUI.mbIsOpen) {
                shootDollUI.StopBgMusic();
            }
        };
        egret.lifecycle.onResume = function () {
            var shootDollUI = UIManager.GetUIObj(UIName.ShootDollUI);
            if (shootDollUI && shootDollUI.mbIsOpen) {
                shootDollUI.PlayBgMusic();
            }
        };
    };
    return LifeCycle;
}());
__reflect(LifeCycle.prototype, "LifeCycle");
//# sourceMappingURL=LifeCycle.js.map