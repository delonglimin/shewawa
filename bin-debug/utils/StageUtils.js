var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var StageUtils = (function () {
    function StageUtils() {
    }
    StageUtils.GetInstance = function () {
        if (!this.instance) {
            this.instance = new StageUtils();
        }
        return this.instance;
    };
    StageUtils.prototype.Init = function (stage) {
        this.stage = stage;
    };
    Object.defineProperty(StageUtils.prototype, "stageWidth", {
        get: function () {
            return this.stage.stageWidth;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StageUtils.prototype, "stageHeight", {
        get: function () {
            return this.stage.stageHeight;
        },
        enumerable: true,
        configurable: true
    });
    StageUtils.instance = null;
    return StageUtils;
}());
__reflect(StageUtils.prototype, "StageUtils");
//# sourceMappingURL=StageUtils.js.map