var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var SoundManager = (function () {
    function SoundManager() {
    }
    SoundManager.Clear = function () {
        this.msSoundMap = {};
    };
    /**创建背景音乐*/
    SoundManager.CreateSoundMusic = function (soundName, callBack) {
        this.CreateSound(soundName, egret.Sound.MUSIC, callBack);
    };
    /**创建音效 */
    SoundManager.CreateSoundEffect = function (soundName, callBack) {
        this.CreateSound(soundName, egret.Sound.EFFECT, callBack);
    };
    SoundManager.CreateSound = function (soundName, type, callBack) {
        var sound = new egret.Sound;
        sound.type = type;
        sound.addEventListener(egret.Event.COMPLETE, function () {
            if (callBack)
                callBack(sound);
            // this.mBgMusicChannel.volume = GameData.GetInstance().GetBgMusicOpen() ? .8 : 0;
            // this.mBgMusicChannel.addEventListener(egret.Event.SOUND_COMPLETE, ()=>{}, this);
        }, this);
        sound.load("resource/sound/" + soundName);
    };
    SoundManager.PlaySoundEffect = function (soundName, startTime, loops) {
        if (startTime === void 0) { startTime = 0; }
        if (loops === void 0) { loops = 0; }
        this.PlaySound(soundName, egret.Sound.EFFECT, startTime, loops);
    };
    SoundManager.StopSoundEffect = function (soundName) {
        this.StopSound(soundName, egret.Sound.EFFECT);
    };
    SoundManager.PlaySoundMusic = function (soundName, startTime, loops) {
        if (startTime === void 0) { startTime = 0; }
        if (loops === void 0) { loops = 0; }
        this.PlaySound(soundName, egret.Sound.MUSIC, startTime, loops);
    };
    SoundManager.StopSoundMusic = function (soundName) {
        this.StopSound(soundName, egret.Sound.MUSIC);
    };
    SoundManager.PlaySound = function (soundName, type, startTime, loops) {
        var _this = this;
        if (startTime === void 0) { startTime = 0; }
        if (loops === void 0) { loops = 0; }
        var typeMap = this.msSoundMap[type];
        if (!typeMap) {
            this.msSoundMap[type] = {};
            typeMap = this.msSoundMap[type];
        }
        var soundMap = typeMap[soundName];
        if (!soundMap) {
            typeMap[soundName] = {};
            soundMap = typeMap[soundName];
        }
        var tempSound = soundMap.sound;
        var sound = null;
        if (tempSound)
            sound = tempSound;
        if (!sound) {
            this.CreateSound(soundName, type, function (s) {
                sound = s;
                var channel = sound.play(startTime, loops);
                channel.volume = _this.GetVolume(type);
                soundMap.sound = sound;
                soundMap.channel = channel;
            });
        }
        else {
            var channel = sound.play(startTime, loops);
            channel.volume = this.GetVolume(type);
            soundMap.sound = sound;
            soundMap.channel = channel;
        }
    };
    SoundManager.StopSound = function (soundName, type) {
        var typeMap = this.msSoundMap[type];
        if (!typeMap)
            return;
        var soundMap = typeMap[soundName];
        if (!soundMap)
            return;
        var tempChannel = soundMap.channel;
        var channel = null;
        if (tempChannel)
            channel = tempChannel;
        if (channel)
            channel.stop();
    };
    SoundManager.GetVolume = function (type) {
        if (type == egret.Sound.MUSIC) {
            return GameData.GetInstance().GetBgMusicOpen() ? 1 : 0;
        }
        if (type == egret.Sound.EFFECT) {
            return GameData.GetInstance().GetSoundOpen() ? 1 : 0;
        }
        return 1;
    };
    SoundManager.msSoundMap = {};
    return SoundManager;
}());
__reflect(SoundManager.prototype, "SoundManager");
//# sourceMappingURL=SoundManager.js.map